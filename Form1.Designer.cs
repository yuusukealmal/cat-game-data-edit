﻿namespace BCSFE
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.transfer_code = new System.Windows.Forms.TextBox();
            this.confirmation_code = new System.Windows.Forms.TextBox();
            this.lb_cc = new System.Windows.Forms.Label();
            this.lb_version = new System.Windows.Forms.Label();
            this.lb_acc = new System.Windows.Forms.Label();
            this.lb_pass = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.kr = new System.Windows.Forms.RadioButton();
            this.en = new System.Windows.Forms.RadioButton();
            this.tw = new System.Windows.Forms.RadioButton();
            this.jp = new System.Windows.Forms.RadioButton();
            this.version = new System.Windows.Forms.ComboBox();
            this.check = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // transfer_code
            // 
            this.transfer_code.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.transfer_code.Location = new System.Drawing.Point(444, 194);
            this.transfer_code.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.transfer_code.MaxLength = 9;
            this.transfer_code.Name = "transfer_code";
            this.transfer_code.Size = new System.Drawing.Size(310, 40);
            this.transfer_code.TabIndex = 2;
            this.transfer_code.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.transfer_code.TextChanged += new System.EventHandler(this.transfer_code_TextChanged);
            // 
            // confirmation_code
            // 
            this.confirmation_code.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.confirmation_code.Location = new System.Drawing.Point(444, 463);
            this.confirmation_code.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.confirmation_code.MaxLength = 4;
            this.confirmation_code.Name = "confirmation_code";
            this.confirmation_code.Size = new System.Drawing.Size(310, 40);
            this.confirmation_code.TabIndex = 3;
            this.confirmation_code.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lb_cc
            // 
            this.lb_cc.BackColor = System.Drawing.Color.Transparent;
            this.lb_cc.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lb_cc.Location = new System.Drawing.Point(62, 28);
            this.lb_cc.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lb_cc.Name = "lb_cc";
            this.lb_cc.Size = new System.Drawing.Size(310, 75);
            this.lb_cc.TabIndex = 4;
            this.lb_cc.Text = "請選擇國家版本";
            this.lb_cc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_version
            // 
            this.lb_version.BackColor = System.Drawing.Color.Transparent;
            this.lb_version.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lb_version.Location = new System.Drawing.Point(74, 363);
            this.lb_version.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lb_version.Name = "lb_version";
            this.lb_version.Size = new System.Drawing.Size(310, 75);
            this.lb_version.TabIndex = 5;
            this.lb_version.Text = "請選擇遊戲版本";
            this.lb_version.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_acc
            // 
            this.lb_acc.BackColor = System.Drawing.Color.Transparent;
            this.lb_acc.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lb_acc.Location = new System.Drawing.Point(444, 28);
            this.lb_acc.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lb_acc.Name = "lb_acc";
            this.lb_acc.Size = new System.Drawing.Size(310, 75);
            this.lb_acc.TabIndex = 6;
            this.lb_acc.Text = "請輸入轉移號碼";
            this.lb_acc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_pass
            // 
            this.lb_pass.BackColor = System.Drawing.Color.Transparent;
            this.lb_pass.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lb_pass.Location = new System.Drawing.Point(444, 363);
            this.lb_pass.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lb_pass.Name = "lb_pass";
            this.lb_pass.Size = new System.Drawing.Size(310, 75);
            this.lb_pass.TabIndex = 7;
            this.lb_pass.Text = "請輸入驗證號碼";
            this.lb_pass.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.kr);
            this.groupBox1.Controls.Add(this.en);
            this.groupBox1.Controls.Add(this.tw);
            this.groupBox1.Controls.Add(this.jp);
            this.groupBox1.Location = new System.Drawing.Point(66, 114);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.groupBox1.Size = new System.Drawing.Size(310, 174);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // kr
            // 
            this.kr.AutoSize = true;
            this.kr.Location = new System.Drawing.Point(185, 117);
            this.kr.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.kr.Name = "kr";
            this.kr.Size = new System.Drawing.Size(89, 32);
            this.kr.TabIndex = 3;
            this.kr.TabStop = true;
            this.kr.Text = "韓版";
            this.kr.UseVisualStyleBackColor = true;
            // 
            // en
            // 
            this.en.AutoSize = true;
            this.en.Location = new System.Drawing.Point(36, 117);
            this.en.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.en.Name = "en";
            this.en.Size = new System.Drawing.Size(89, 32);
            this.en.TabIndex = 2;
            this.en.TabStop = true;
            this.en.Text = "英版";
            this.en.UseVisualStyleBackColor = true;
            // 
            // tw
            // 
            this.tw.AutoSize = true;
            this.tw.Location = new System.Drawing.Point(185, 43);
            this.tw.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tw.Name = "tw";
            this.tw.Size = new System.Drawing.Size(89, 32);
            this.tw.TabIndex = 1;
            this.tw.TabStop = true;
            this.tw.Text = "台版";
            this.tw.UseVisualStyleBackColor = true;
            // 
            // jp
            // 
            this.jp.AutoSize = true;
            this.jp.ForeColor = System.Drawing.SystemColors.ControlText;
            this.jp.Location = new System.Drawing.Point(36, 43);
            this.jp.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.jp.Name = "jp";
            this.jp.Size = new System.Drawing.Size(89, 32);
            this.jp.TabIndex = 0;
            this.jp.TabStop = true;
            this.jp.Text = "日版";
            this.jp.UseVisualStyleBackColor = true;
            // 
            // version
            // 
            this.version.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.version.FormattingEnabled = true;
            this.version.Location = new System.Drawing.Point(102, 449);
            this.version.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.version.Name = "version";
            this.version.Size = new System.Drawing.Size(262, 35);
            this.version.TabIndex = 10;
            this.version.SelectedIndexChanged += new System.EventHandler(this.version_SelectedIndexChanged);
            // 
            // check
            // 
            this.check.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.check.Location = new System.Drawing.Point(776, 452);
            this.check.Name = "check";
            this.check.Size = new System.Drawing.Size(100, 55);
            this.check.TabIndex = 11;
            this.check.Text = "確認";
            this.check.UseVisualStyleBackColor = false;
            this.check.Click += new System.EventHandler(this.check_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(405, 263);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(414, 100);
            this.label1.TabIndex = 12;
            this.label1.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(888, 559);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.check);
            this.Controls.Add(this.version);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lb_pass);
            this.Controls.Add(this.lb_acc);
            this.Controls.Add(this.lb_version);
            this.Controls.Add(this.lb_cc);
            this.Controls.Add(this.confirmation_code);
            this.Controls.Add(this.transfer_code);
            this.Font = new System.Drawing.Font("新細明體", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox transfer_code;
        private System.Windows.Forms.TextBox confirmation_code;
        private System.Windows.Forms.Label lb_cc;
        private System.Windows.Forms.Label lb_version;
        private System.Windows.Forms.Label lb_acc;
        private System.Windows.Forms.Label lb_pass;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton kr;
        private System.Windows.Forms.RadioButton en;
        private System.Windows.Forms.RadioButton tw;
        private System.Windows.Forms.RadioButton jp;
        private System.Windows.Forms.ComboBox version;
        private System.Windows.Forms.Button check;
        private System.Windows.Forms.Label label1;
    }
}

