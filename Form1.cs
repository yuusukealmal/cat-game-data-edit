﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BCSFE.data_download;
using System.Net.Http;
using BCSFE.parse;
using Newtonsoft.Json;

namespace BCSFE
{
    public partial class Form1 : Form
    {
        private Info user;
        private List<String> v = new List<string> {
            "12.3.0", "12.2.2", "12.2.1","130201", "12.2.0", "12.1.1", "12.0.0",
            "11.10.0", "11.9.2", "11.9.0", "11.8.0", "11.7.0", "11.6.0", "11.5.0", "11.4.0", "11.3.0", "11.2.0", "11.1.0", "11.0.0",
            "10.10.0", "10.9.0", "10.8.0", "10.7.0", "10.6.0", "10.5.0", "10.4.0", "10.3.0", "10.2.0", "10.1.0", "10.0.0",
            "9.10.0", "9.9.0", "9.8.0", "9.7.0", "9.6.0", "9.5.0", "9.4.0", "9.3.0", "9.2.0", "9.1.0", "9.0.0",
            "8.10.5", "8.10.0", "8.9.0", "8.8.0", "8.7.5", "8.7.0", "8.6.0", "8.5.0", "8.4.0", "8.3.0"
        };
        public static String baseURL = "https://nyanko-save.ponosgames.com";
    public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            transfer_code.Text = "11dd4db03";
            confirmation_code.Text = "3719";
            this.Text = "SMALL THING @made by JAR";
            label1.Text = " ";
            var fs = File.OpenRead("icon.ico");
            Icon = new Icon(fs);
            fs.Close();
            BackgroundImage = Image.FromFile("bg1.jpg");
            reset();
            version.SelectedText="12.3.0"; ;
            jp.CheckedChanged += new EventHandler(radiobtn_oncheck);
            tw.CheckedChanged += new EventHandler(radiobtn_oncheck);
            en.CheckedChanged += new EventHandler(radiobtn_oncheck);
            kr.CheckedChanged += new EventHandler(radiobtn_oncheck);
        }

        void reset()
        {
            check.Focus();
            lb_cc.Visible = true; lb_version.Enabled = false; lb_acc.Enabled = false; lb_pass.Enabled = false;
            groupBox1.Enabled = true;
            jp.Checked = false; tw.Checked = false; en.Checked = false; kr.Checked = false;
            version.Enabled = false; transfer_code.Enabled = false; confirmation_code.Enabled = false;
            version.Items.Clear();
        }

        private RadioButton preBtn;
        private void radiobtn_oncheck(object sender, EventArgs e)
        {
            RadioButton radioButton = sender as RadioButton;

            if (radioButton != null && radioButton.Checked)
            {
                radioButton.ForeColor = Color.Fuchsia;
                lb_version.Enabled = true;
                version.Enabled = true;

                if (preBtn != null && preBtn != radioButton)
                {
                    preBtn.ForeColor = SystemColors.ControlText;
                }
                preBtn = radioButton;
            }
            /*
             jp:12.3.0
             tw:12.2.1
             en:12.2.1
             kr:12.2.1
             */
            switch (radioButton.Name)
            {
                case "jp":
                case "tw":
                    version.Items.Clear();
                    version.Items.Add("請選擇版本");
                    foreach (var i in v)
                    {
                        version.Items.Add(i);
                    }
                    break;
                //case "tw":
                case "en":
                case "kr":
                    version.Items.Clear();
                    version.Items.Add("請選擇版本");
                    for (int i = 2; i < v.Count; i++)
                    {
                        version.Items.Add(v[i]);
                    }
                    version.SelectedIndex = 1;
                    break;
                default:
                    break;
            }
        }

        private void version_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (version.SelectedItem.ToString() != "請選擇版本") //&& version.SelectedItem.ToString() != "")
            {
                lb_acc.Enabled = true;
                transfer_code.Enabled = true;
            }
            else
            {
                lb_acc.Enabled = false;
                transfer_code.Enabled = false;
            }
        }

        private void transfer_code_TextChanged(object sender, EventArgs e)
        {
            if (transfer_code.Text.Length < 9)
            {
                lb_pass.Enabled = false;
                confirmation_code.Enabled = false;
            }
            else
            {
                lb_pass.Enabled = true;
                confirmation_code.Enabled = true;
            }
        }

        private async void check_Click(object sender, EventArgs e)
        {
            if (!(jp.Checked || tw.Checked || en.Checked || kr.Checked))
            {
                MessageBox.Show("請選擇國家");
            }
            else if (version.SelectedItem.ToString() == "請選擇版本" || version.SelectedItem.ToString() == "")
            {
                MessageBox.Show("請選擇版本");
            }
            else if (transfer_code.Text.Length < 9)
            {
                MessageBox.Show("請輸入完整轉移碼");
            }
            else if (confirmation_code.Text.Length < 4)
            {
                MessageBox.Show("請輸入完整驗證碼");
            }
            else
            {
                user = new Info();
                user.CC = jp.Checked ? "ja" : tw.Checked ? "tw" : en.Checked ? "en" : "kr";
                user.Version = version.SelectedItem.ToString();
                user.TransferCode = transfer_code.Text;
                user.ConfirmationCode = confirmation_code.Text;

                Req req = new Req();
                HttpResponseMessage response = await req.download_save(user.CC, user.TransferCode, user.ConfirmationCode, user.Version);
                byte[] save_data = await response.Content.ReadAsByteArrayAsync();
                File.WriteAllBytes("save_data", save_data);
                bool save = if_save.check_save(save_data);
                if (save == true)
                {
                    var headers = response.Headers;
                    var save_stats = start_parse.Start_parse(save_data, user.CC);
                    MessageBox.Show("Success");
                    File.WriteAllText("save_data.json", JsonConvert.SerializeObject(save_stats));
                    // save_stats["token"] = headers.GetValues("nyanko-password-refresh-token");
                    /*var info = UserInfo(save_stats["inquiry_code"]);
                    info.set_password(headers.GetValues("nyanko-password"));
                    save_data = serialise_save.start_serialize(save_stats);
                    save_data = patcher.patch_save_data(save_data, user.CC);
                    path = helper.save_file(
                        "Save save data",
                        helper.get_save_file_filetype(),
                        helper.get_save_path_home(),

                    );*/
                    File.WriteAllBytes("save_data", save_data);
                }
                else
                {
                    MessageBox.Show("Fail");
                }
            }
        }
    }
}
