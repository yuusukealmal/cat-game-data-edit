﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCSFE
{
    internal class Info
    {
        public string CC { get; set; }
        public string Version { get; set; }
        public string TransferCode { get; set; }
        public string ConfirmationCode { get; set; }
    }
}
