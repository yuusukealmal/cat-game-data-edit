﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCSFE
{
    internal class client_info
    {
        public static Dictionary<string, object> GetClientInfo(string countryCode, string gameVersion)
        {
            countryCode = countryCode.Replace("jp", "ja");

            var data = new Dictionary<string, object>
    {
        {
            "clientInfo", new Dictionary<string, object>
            {
                {
                    "client", new Dictionary<string, object>
                    {
                        {"countryCode", countryCode},
                        {"version", gameVersion}
                    }
                },
                {
                    "device", new Dictionary<string, object>
                    {
                        {"model", "SM-G955F"}
                    }
                },
                {
                    "os", new Dictionary<string, object>
                    {
                        {"type", "android"},
                        {"version", "9"}
                    }
                }
            }
        },
        {"nonce",hex.RandomHexString(32)}
    };

            return data;
        }
    }
}
