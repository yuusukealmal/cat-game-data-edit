﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BCSFE.data_download
{
    internal class Req
    {
        public async Task<HttpResponseMessage> download_save(string country_code, string transfer_code, string confirmation_code, string game_version)
        {
            country_code = country_code.Replace("jp", "ja");
            string url = Form1.baseURL + "/v2/transfers/" + transfer_code + "/reception";
            Dictionary<string, object> data = client_info.GetClientInfo(country_code, game_version);
            data["pin"] = confirmation_code;
            // data["isPasswordRefresh"] = true;
            string data_s = JsonConvert.SerializeObject(data, Formatting.None).Replace(" ", "");
            // data["isPasswordRefresh"] = true;
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    var content = new StringContent(data_s, Encoding.UTF8, "application/json");
                    client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("gzip"));
                    client.DefaultRequestHeaders.Connection.Add("keep-alive");
                    client.DefaultRequestHeaders.UserAgent.ParseAdd("Dalvik/2.1.0 (Linux; U; Android 9; SM-G955F Build/N2G48B)");
                    var response = await client.PostAsync(url, content);
                    return response;
                }
            }
            catch (HttpRequestException e)
            {
                throw new Exception("Error getting save: " + e.Message, e);
            }
        }
    }
}
