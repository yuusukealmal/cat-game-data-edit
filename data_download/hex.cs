﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCSFE
{
    internal class hex
    {
        public static string RandomHexString(int length)
        {
            var random = new Random();
            const string chars = "0123456789abcdef";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
