﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCSFE.data_download
{
    internal class if_save
    {
        public static bool check_save(byte[] saveData)
        {
            try
            {
                string saveDataString = Encoding.UTF8.GetString(saveData);
                JsonConvert.DeserializeObject(saveDataString);
            }
            catch (JsonException)
            {
                return true;
            }
            catch (DecoderFallbackException)
            {
                return true;
            }
            return false;
        }
    }
}
