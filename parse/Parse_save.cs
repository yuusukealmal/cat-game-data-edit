﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BCSFE.parse
{
    internal class Parse_save
    {
        public static int address;
        public static byte[] save_data_g;
        public static void set_address(int val)
        {
            address = val;
        }
        public static Dictionary<string, int> next_int_len(int number)
        {
            if (number < 0)
            {
                throw new Exception("Invalid number");
            }
            if (save_data_g == null)
            {
                throw new Exception("Invalid save data");
            }
            if (number > save_data_g.Length)
            {
                throw new Exception("Byte length is greater than the length of the save data");
            }
            int val = convert_little(save_data_g.Skip(address).Take(number).ToArray());
            
            Dictionary<string, int> data = new Dictionary<string, int>();
            set_address(address + number);
            data.Add("Value", val);
            data.Add("Length", number);
            return data;
        }
        public static Dictionary<string, int> next_int_len2(int number)
        {
            if (number < 0)
            {
                throw new Exception("Invalid number");
            }
            if (save_data_g == null)
            {
                throw new Exception("Invalid save data");
            }
            if (number > save_data_g.Length)
            {
                throw new Exception("Byte length is greater than the length of the save data");
            }
            int val = convert_little(save_data_g.Skip(address).Take(number).ToArray());

            Dictionary<string, int> data = new Dictionary<string, int>();
            set_address(address + number);
            // if (val == 0) MessageBox.Show("");
            data.Add("Value", val);
            data.Add("Length", number);
            return data;
        }
        public static int convert_little(byte[] byteData)
        {
        if (byteData.Length == 0) return 0;
        if (byteData.Length == 1)
        {
            byte[] byteData2 = { byteData[0], 0, 0, 0 };
            return BitConverter.ToInt32(byteData2, 0);
            }
        if (byteData.Length == 2)
        {
            byte[] byteData2 = { byteData[0], byteData[1], 0, 0 };
        return BitConverter.ToInt32(byteData2, 0);
            }
        return BitConverter.ToInt32(byteData,0);
        }
        public static int next_int(int number)
        {
            return next_int_len(number)["Value"];
        }
        public static int find_date()
        {
            for (int i = 0; i < 100; i++)
            {
                int val = next_int(4);
                if (val >= 2000 && val <= 3000) return address - 4;
            }
            throw new Exception("Could not find date");
        }
        public static bool get_dst(byte[] save_data, int offset)
        {
            bool dst = false;
            if (save_data[offset] >= 15 && save_data[offset] <= 20) dst = true;
            else if (save_data[offset - 1] >= 15 && save_data[offset - 1] <= 20) dst = false; //# Offset in jp due to no dst
            return dst;
        }
        public static double get_double()
        {
            if (save_data_g == null) throw new Exception("No save data loaded");
            byte[] data = save_data_g.Skip(address).Take(8).ToArray();
            double val = BitConverter.ToDouble(data, 0);
            set_address(address + 8);
            return val;
        }
        public static Dictionary<string, dynamic> get_time_data_skip(bool dst_flag)
        {
            int year = next_int(4);
            int year_2 = next_int(4);

            int month = next_int(4);
            int month_2 = next_int(4);

            int day = next_int(4);
            int day_2 = next_int(4);

            double timeStamp = get_double();

            int hour = next_int(4);
            int minute = next_int(4);
            int second = next_int(4);
            int dst = 0;
            if (dst_flag == true) dst = next_int(1);
            DateTime time = new DateTime(year, month, day, hour, minute, second);
            return new Dictionary<string, dynamic>
    {
        { "time", time.ToString("s") },
        { "time_stamp", timeStamp },
        { "dst", dst },
        { "duplicate", new Dictionary<string, int>{ {"yy", year_2}, {"mm", month_2}, {"dd", day_2} } }
    };
        }
        public static List<int> get_length_data(int length_bytes = 4, int separator = 4, int? length = null)
        {
            List<int> data = new List<int>();
            if (length == null) length = next_int(length_bytes);
            if (save_data_g == null) throw new Exception("Invalid save data");
            if (length > save_data_g.Length) throw new Exception("Length too large");
            for (int i = 0; i < length; i++)
            {
                data.Add(next_int(separator));
            }
            return data;
        }
        public static List<List<int>> get_equip_slots()
        {
            int length = next_int(1);
            List<int> data = get_length_data(1, length: length * 10);
            List<List<int>> slots = new List<List<int>>();
            for (int i = 0; i < length; i++)
            {
                int start_pos = 10 * i;
                int end_pos = 10 * (i + 1);
                slots.Add(data.GetRange(start_pos, end_pos - start_pos));
            }
            return slots;
        }
        public static Dictionary<string, dynamic> get_main_story_levels()
        {
            List<int> chapter_progress = new List<int>();
            for (int i = 0; i < 10; i++)
            {
                chapter_progress.Add(next_int(4));
            }
            List<List<int>> times_cleared = new List<List<int>>();
            for (int i = 0; i < 10; i++)
            {
                List<int> chapter_times = new List<int>();
                for (int j = 0; j < 51; j++)
                {
                    chapter_times.Add(next_int(4));
                }
                times_cleared.Add(chapter_times);
            }
            Dictionary<string, dynamic> chapter_data = new Dictionary<string, dynamic>()
    {
        { "Chapter Progress", chapter_progress },
        { "Times Cleared", times_cleared }
    };
            return chapter_data;
        }
        public static List<List<int>> get_treasures()
        {
            List<List<int>> treasures = new List<List<int>>();
            for (int i = 0; i < 10; i++)
            {
                List<int> chapter = new List<int>();
                for (int j = 0; j < 51; j++)
                {
                    chapter.Add(next_int(4));
                }
                treasures.Add(chapter);
            }
            return treasures;
        }
        public static Dictionary<string, dynamic> get_cat_upgrades()
        {
            int length = next_int(4);
            List<int> data = get_length_data(4, 2, length * 2);
            List<int> base_levels = data.Where((n, i) => i % 2 == 1).ToList();
            List<int> plus_levels = data.Where((n, i) => i % 2 == 0).ToList();
            Dictionary<string, dynamic> data_dict = new Dictionary<string, dynamic>()
{
    { "Base", base_levels },
    { "Plus", plus_levels }
};
            return data_dict;
        }
        public static Dictionary<string, dynamic> get_blue_upgrades()
        {
            int length = 11;
            List<int> data = get_length_data(4, 2, length * 2);
            List<int> base_levels = data.Where((n, i) => i % 2 == 1).ToList();
            List<int> plus_levels = data.Where((n, i) => i % 2 == 0).ToList();
            Dictionary<string, dynamic> data_dict = new Dictionary<string, dynamic>()
{
    { "Base", base_levels },
    { "Plus", plus_levels }

};
            return data_dict;
        }
        public static string get_time_data(bool dstFlag)
        {
            if (dstFlag)
            {
                _ = next_int(1);
            }
            int year = next_int(4);
            int month = next_int(4);
            int day = next_int(4);
            int hour = next_int(4);
            int minute = next_int(4);
            int second = next_int(4);

            DateTime time = new DateTime(year, month, day, hour, minute, second);
            return $"{time:yyyy-MM-dd}";
        }
        public static string get_utf8_string(int? length = null)
        {
            var data = get_length_data(4, 1, length);
            var utf8 = Encoding.UTF8.GetString(data.Select(b => (byte)b).ToArray());
            return utf8;
        }
        public static int read_variable_length_int()
        {
            int i = 0;
            for (int j = 0; j < 4; j++)
            {
                int i_3 = i << 7;
                int read = next_int(1);
                i = i_3 | (read & 127);
                if ((read & 128) == 0)
                {
                    return i;
                }
            }
            return i;
        }
        public static (Dictionary<int, int>, Dictionary<int, int>) load_bonus_hash()
        {
            int length_1 = read_variable_length_int();
            Dictionary<int, int> data_1 = new Dictionary<int, int>();
            for (int i = 0; i < length_1; i++)
            {
                int key = read_variable_length_int();
                int val = read_variable_length_int();
                data_1[key] = val;
            }
            int length_2 = read_variable_length_int();
            Dictionary<int, int> data_2 = new Dictionary<int, int>();
            for (int i = 0; i < length_2; i++)
            {
                int key = read_variable_length_int();
                int val = next_int(1);
                data_2[key] = val;
            }
            return (data_1, data_2);
        }
        public static void skip(int number)
        {
            set_address(address + number);
        }
        public static IEnumerable<List<T>> chunks<T>(List<T> lst, int chunkLen)
        {
            for (int i = 0; i < lst.Count; i += chunkLen)
            {
                yield return lst.GetRange(i, Math.Min(chunkLen, lst.Count - i));
            }
        }
        public static Dictionary<string, dynamic> get_event_stages_current()
        {
            int unknown_val = next_int(1);
            int total_sub_chapters = next_int(2) * unknown_val;
            int stars_per_sub_chapter = next_int(1);
            int stages_per_sub_chapter = next_int(1);

            List<int> clear_progress = get_length_data(1, 1, total_sub_chapters * stars_per_sub_chapter);
            List<List<int>> clear_progress_ = (List<List<int>>)(chunks(clear_progress, stars_per_sub_chapter));
            Dictionary<string, dynamic> dict = new Dictionary<string, dynamic>()
{
    { "Clear", clear_progress_ },
    { "unknown", unknown_val},
    { "total", total_sub_chapters },
    { "stages", stages_per_sub_chapter },
    { "stars", stars_per_sub_chapter}
};
            return dict;
        }
        public static Dictionary<string, dynamic> get_event_stages(Dictionary<string, dynamic> lengths)
        {
            var total_sub_chapters = (int)lengths["total"];
            var stars_per_sub_chapter = (int)lengths["stars"];
            var stages_per_sub_chapter = (int)lengths["stages"];
            List<int> clear_progress = get_length_data(1, 1, total_sub_chapters * stars_per_sub_chapter);
            List<int> clear_amount = get_length_data(1, 2, total_sub_chapters * stages_per_sub_chapter * stars_per_sub_chapter);
            List<int> unlock_next = get_length_data(1, 1, total_sub_chapters * stars_per_sub_chapter);
            List<List<int>> clear_progress_ = (List<List<int>>)chunks(clear_progress, stars_per_sub_chapter);
            List<List<int>> clear_amount_ = (List<List<int>>)chunks(clear_amount, stages_per_sub_chapter * stars_per_sub_chapter);
            List<List<int>> unlock_next_ = (List<List<int>>)chunks(unlock_next, stars_per_sub_chapter);
            List<List<List<int>>> clear_amount_sep = new List<List<List<int>>>();
            for (int i = 0; i < clear_amount_.Count; i++)
            {
                var clear_amount_val = clear_amount_[i];
                List<List<int>> sub_chapter_clears = new List<List<int>>();
                for (int j = 0; j < stars_per_sub_chapter; j++)
                {
                    var sub_clears = clear_amount_val.Where((n, index) => index % stars_per_sub_chapter == j&&index>j).ToList();
                    sub_chapter_clears.Add(sub_clears);
                }
                clear_amount_sep.Add(sub_chapter_clears);
            }
            return new Dictionary<string, dynamic>
    {
        {
            "Value",
            new Dictionary<string, dynamic>
            {
                { "clear_progress", clear_progress_ },
                { "clear_amount", clear_amount_sep },
                { "unlock_next", unlock_next_ }
            }
        },
        { "Lengths", lengths }
    };
        }
        public static List<double> get_length_doubles(int length_bytes = 4, int? length = null)
        {
            List<double> data = new List<double>();
            if (length == null) length = next_int(length_bytes);
            if (save_data_g == null) throw new Exception("Invalid save data");
            if (length > save_data_g.Length) throw new Exception("Length too large");
            for (int i = 0; i < length; i++)
            {
                data.Add(get_double());
            }
            return data;
        }
        public static Dictionary<string, int> generate_empty_len(int length)
        {
            Dictionary<string, int> data = new Dictionary<string, int>();
            data["Length"] = length;
            data["Value"] = 0;
            return data;
        }
        public static Dictionary<string, dynamic> get_event_timed_scores()
        {
            int total_subchapters = next_int(4);
            int stages_per_subchapter = next_int(4);
            int stars = next_int(4);
            List<int> score = get_length_data(4, 4, total_subchapters * stages_per_subchapter * stars);
            List<List<int>> score_ = (List<List<int>>)chunks(score, stars * stages_per_subchapter);
            Dictionary<string, dynamic> data = new Dictionary<string, dynamic>()
{
    {"Score", score_ },
    {"total", total_subchapters},
    {"stages", stages_per_subchapter },
    {"stars", stars}
};
            return data;

        }
        public static int clamp(int value, int min_value, int max_value)
        {
            return Math.Max((Math.Min(value, max_value)), min_value);
        }
        public static int clamp_int(int value)
        {
            return clamp(value, 0, (int)Math.Pow(2, 31) - 1);
        }
        public static Dictionary<string, dynamic> frames_to_time(int frames)
        {
            frames = clamp_int(frames);
            int seconds = frames / 30;
            frames = frames % 30;
            int minutes = seconds / 60;
            seconds = seconds % 60;
            int hours = minutes / 60;
            minutes = minutes % 60;
            Dictionary<string, dynamic> data = new Dictionary<string, dynamic>()
{
    {"hh", hours },
    {"mm", minutes },
    {"ss", seconds },
    {"frames", frames }
};
            return data;
        }
        public static Dictionary<string, dynamic> get_play_time()
        {
            Dictionary<string, int> raw_val = next_int_len(4);
            int frames = raw_val["Value"];
            //Dictionary<string, dynamic> play_time_data = new Dictionary<string, dynamic>();
            //return play_time_data;
            return frames_to_time(frames);
        }
        public static Dictionary<string, dynamic> seconds_to_time(int seconds)
        {
            int minutes = seconds / 60;
            seconds = seconds % 60;
            int hours = minutes / 60;
            minutes = minutes % 60;
            Dictionary<string, dynamic> data = new Dictionary<string, dynamic>()
{
    {"hh", hours },
    {"mm", minutes },
    {"ss", seconds }
};
            return data;
        }
        public static Dictionary<int, int> get_login_bonuses()
        {
            int length = next_int(4);
            Dictionary<int, int> data = new Dictionary<int, int>();
            for (int i = 0; i < length; i++)
            {
                int login_id = next_int(4);
                data[login_id] = next_int(4);
            }
            return data;
        }
        public static List<Dictionary<string, dynamic>> get_purchase_receipts()
        {
            int total_strs = next_int(4);
            List<Dictionary<string, dynamic>> data = new List<Dictionary<string, dynamic>>();
            for (int i = 0; i < total_strs; i++)
            {
                Dictionary<string, dynamic> data_dict = new Dictionary<string, dynamic>();
                data_dict["unknown_4"] = next_int(4);
                int strings = next_int(4);
                List<dynamic> item_packs = new List<dynamic>();
                for (int j = 0; j < strings; j++)
                {
                    Dictionary<string, dynamic> string_dict = new Dictionary<string, dynamic>();
                    string_dict["Value"] = get_utf8_string();
                    string_dict["unknown_1"] = next_int(1);
                    item_packs.Add(string_dict);
                }
                data_dict["item_packs"] = item_packs;
                data.Add(data_dict);
            }
            return data;
        }
        public static List<Dictionary<string, int>> get_data_before_outbreaks()
        {
            List<Dictionary<string, int>> data = new List<Dictionary<string, int>>();
            Dictionary<string, int> length = next_int_len(4);
            data.Add(length);
            for (int i = 0; i < length["Value"]; i++)
            {
                var length_2 = next_int_len(4);
                data.Add(length_2);

                var length_3 = next_int_len(4);
                data.Add(length_3);

                for (int j = 0; j < length_3["Value"]; j++)
                {
                    var val_1_ = next_int_len(4);
                    data.Add(val_1_);

                    var val_2_ = next_int_len(4);
                    data.Add(val_2_);
                }
            }

            length = next_int_len(4);
            data.Add(length);

            for (int i = 0; i < length["Value"]; i++)
            {
                var val_1_ = next_int_len(4);
                data.Add(val_1_);

                var val_2_ = next_int_len(4);
                data.Add(val_2_);
            }

            length = next_int_len(4);
            data.Add(length);

            for (int i = 0; i < length["Value"]; i++)
            {
                var val_1_ = next_int_len(4);
                data.Add(val_1_);

                var val_2_ = next_int_len(4);
                data.Add(val_2_);
            }

            length = next_int_len(4);
            data.Add(length);

            var val_1 = next_int_len(4);
            data.Add(val_1);

            for (int i = 0; i < length["Value"]; i++)
            {
                var val_2_ = next_int_len(8);
                data.Add(val_2_);

                var val_3 = next_int_len(4);
                data.Add(val_3);
            }

            var gv_56 = next_int_len(4); //0x38
            data.Add(gv_56);

            var _val_1 = next_int_len(1);
            data.Add(_val_1);

            length = next_int_len(4);
            data.Add(length);

            var val_2 = next_int_len(4);
            data.Add(val_2);

            for (int i = 0; i < length["Value"]; i++)
            {
                var val_3 = next_int_len(4);
                data.Add(val_3);

                var val_4 = next_int_len(4);
                data.Add(val_4);
            }
            return data;
        }
        public static Dictionary<int, dynamic> get_dojo_data_maybe()
        {
            //everything here is speculative and might not be correct
            Dictionary<int, dynamic> dojo_data = new Dictionary<int, dynamic>();
            int total_subchapters = next_int(4);
            for (int i = 0; i < total_subchapters; i++)
            {
                int subchapter_id = next_int(4);
                Dictionary<dynamic, dynamic> subchapter_data = new Dictionary<dynamic, dynamic>();

                int total_stages = next_int(4);
                for (int j = 0; j < total_stages; j++)
                {
                    int stage_id = next_int(4);

                    int score = next_int(4);
                    subchapter_data[stage_id] = score;
                }
                dojo_data[subchapter_id] = subchapter_data;
            }
            return dojo_data;
        }
        public static Dictionary<int, dynamic> get_outbreaks()
        {
            int chapters_count = next_int(4);
            Dictionary<int, dynamic> outbreaks = new Dictionary<int, dynamic>();
            for (int i = 0; i < chapters_count; i++)
            {
                int chapter_id = next_int(4);
                int stages_count = next_int(4);
                Dictionary<dynamic, dynamic> chapter = new Dictionary<dynamic, dynamic>();
                for (int j = 0; j < stages_count; j++)
                {
                    int stage_id = next_int(4);
                    int outbreak_cleared_flag = next_int(1);
                    chapter[stage_id] = outbreak_cleared_flag;
                }
                outbreaks[chapter_id] = chapter;
            }
            return outbreaks;
        }
        public static List<Dictionary<string, int>> get_mission_data_maybe()
        {
            List<Dictionary<string, int>> data = new List<Dictionary<string, int>>();

            var length = next_int_len(4);
            data.Add(length);

            for (int i = 0; i < length["Value"]; i++)
            {
                var val_1 = next_int_len(4);
                data.Add(val_1);

                var val_2 = next_int_len(4);
                data.Add(val_2);
            }
            return data;
        }
        public static dynamic get_unknown_data()
        {
            List<Dictionary<string, int>> data = new List<Dictionary<string, int>>();
            var length = next_int_len(4);
            data.Add(length);

            for (int i = 0; i < length["Value"]; i++)
            {
                var length_2 = next_int_len(4);
                data.Add(length_2);

                var val_1 = next_int_len(4);
                data.Add(val_1);
            }
            var unknown_val_2 = next_int_len(1);
            data.Add(unknown_val_2);

            var length_ = next_int_len(4);
            data.Add(length_);

            for (int i = 0; i < length_["Value"]; i++)
            {
                var val_1 = next_int_len(4);
                data.Add(val_1);

                var val_2 = next_int_len(1);
                data.Add(val_2);
            }
            return data;
        }
        public static (List<(int, int)>, Dictionary<string, int>) get_unlock_popups()
        {
            var length = next_int_len(4);
            var val_1 = next_int_len(4);

            List<(int, int)> data = new List<(int, int)>();

            for (int i = 0; i < length["Value"]; i++)
            {
                int flag = next_int(1);

                int popup_id = next_int(4);
                data.Add((popup_id, flag));
            }
            return (data, val_1);
        }
        public static Dictionary<int, Dictionary<string, dynamic>> get_cat_cannon_data()
        {
            int length = next_int(4);
            Dictionary<int, Dictionary<string, dynamic>> cannon_data = new Dictionary<int, Dictionary<string, dynamic>>();
            for (int i = 0; i < length; i++)
            {
                Dictionary<string, dynamic> cannon = new Dictionary<string, dynamic>();
                int cannon_id = next_int(4);
                int len_val = next_int(4);
                int unlock_flag = next_int(4);
                int effect_level = next_int(4);
                int foundation_level = 0, style_level = 0;
                if (len_val == 4)
                {
                    foundation_level = next_int(4);
                    style_level = next_int(4);
                }
                Dictionary<string, int> levels = new Dictionary<string, int>()
    {
        {"effect", effect_level },
        {"foundation", foundation_level },
        {"style", style_level }
    };

                cannon["levels"] = levels;
                cannon["unlock_flag"] = unlock_flag;
                cannon["len_val"] = len_val;
                cannon_data[cannon_id] = cannon;
            }
            return cannon_data;
        }
        public static List<Dictionary<string, int>> get_data_near_ht()
        {
            List<Dictionary<string, int>> data = new List<Dictionary<string, int>>();

            var val = next_int_len(1);
            data.Add(val);

            for (int i = 0; i < val["Value"]; i++)
            {
                var unknown_val_2 = next_int_len(3);
                data.Add(unknown_val_2);
            }
            var unknown_val_3 = next_int_len(3);
            data.Add(unknown_val_3);
            var gv_64 = next_int_len(4); //0x40
            data.Add(gv_64);

            var length = next_int_len(4);
            data.Add(length);

            var val_1 = next_int_len(4);
            data.Add(val_1);

            Dictionary<string, int> val_3 = new Dictionary<string, int>() { { "Value", 0 } };
            for (int i = 0; i < length["Value"]; i++)
            {
                var val_2 = next_int_len(4);
                data.Add(val_2);

                val_3 = next_int_len(4);
                data.Add(val_3);
            }

            val_1 = next_int_len(4);
            data.Add(val_1);

            Dictionary<string, int> val_4 = new Dictionary<string, int>() { { "Value", 0 } };
            for (int i = 0; i < val_3["Value"]; i++)
            {
                length = next_int_len(4);
                data.Add(length);
                for (int j = 0; j < length["Value"]; j++)
                {
                    var val_2 = next_int_len(4);
                    data.Add(val_2);
                }
                val_4 = next_int_len(4);
                data.Add(val_4);
            }
            val_1 = next_int_len(4);
            data.Add(val_1);

            for (int i = 0; i < val_4["Value"]; i++)
            {
                var val_2 = next_int_len(4);
                data.Add(val_2);

                val_3 = next_int_len(4);
                data.Add(val_3);
            }
            return data;

        }
        public static Dictionary<string, dynamic> get_ht_it_data()
        {
            int total = next_int(4);
            int stars = next_int(4);
            var selected = new List<int>();
            for (int i = 0; i < total; i++)
            {
                for (int j = 0; j < stars; j++)
                {
                    selected.Add(next_int(4));
                }
            }
            List<List<int>> selected_ = (List<List<int>>)(chunks(selected, 4));
            Dictionary<string, dynamic> current_data = new Dictionary<string, dynamic>()
            {
                {"total",total },
                {"stars", stars },
                {"selected", selected_ }
            };

            total = next_int(4);
            stars = next_int(4);

            Dictionary<string, dynamic> progress_data = new Dictionary<string, dynamic>();

            var clear_progress = new List<int>();
            for (int i = 0; i < total; i++)
            {
                for (int j = 0; j < stars; j++)
                {
                    clear_progress.Add(next_int(4));
                }
            }
            List<List<int>> clear_progress_ = (List<List<int>>)(chunks(clear_progress, 4));

            total = next_int(4);
            int stages = next_int(4);

            stars = next_int(4);

            var clear_amount = get_length_data(4, 4, total * stages * stars);
            List<List<int>> clear_amount_ = (List<List<int>>)(chunks(clear_amount, stages * stars));

            List<List<List<int>>> clear_amount_sep = new List<List<List<int>>>();

            foreach (var clear_amount_val in clear_amount_)
            {
                List<List<int>> sub_chapters_clears = new List<List<int>>();
                for (int j = 0; j < stars; j++)
                {
                    var sub_clears = clear_amount_val.Where((n, index) => index % stars == j&&index>j).ToList();
                    sub_chapters_clears.Add(sub_clears);
                }
                clear_amount_sep.Add(sub_chapters_clears);
            }
            Dictionary<string, dynamic> progress_data_ = new Dictionary<string, dynamic>()
            {
                {"total",total },
                {"stars", stars },
                {"clear_progress", clear_progress_ },
                {"clear_amount", clear_amount_sep }
            };
            List<Dictionary<string, int>> data = new List<Dictionary<string, int>>();
            var length = next_int_len(4);
            data.Add(length);

            var length_2 = next_int_len(2);
            data.Add(length_2);

            for (int i = 0; i < length["Value"]; i++)
            {
                for (int j = 0; j < length_2["Value"]; j++)
                {
                    data.Add(next_int_len(4));
                }
            }

            return new Dictionary<string, dynamic>()
            {
                {"data", data },
                {"current", current_data },
                {"progress",  progress_data_}
            };
        }
        public static Dictionary<int, int> get_mission_segment()
        {
            Dictionary<int, int> missions = new Dictionary<int, int>();
            int length = next_int(4);
            for (int i = 0; i < length; i++)
            {
                int mission_id = next_int(4);
                int mission_value = next_int(4);
                missions[mission_id] = mission_value;
            }
            return missions;
        }
        public static Dictionary<string, Dictionary<int, int>> get_mission_data()
        {
            Dictionary<string, Dictionary<int, int>> missions = new Dictionary<string, Dictionary<int, int>>()
            {
                {"states", get_mission_segment() },
                {"requirements", get_mission_segment() },
                {"clear_types", get_mission_segment() },
                {"gamatoto", get_mission_segment() },
                {"nyancombo", get_mission_segment() },
                {"user_rank", get_mission_segment() },
                {"expiry", get_mission_segment() },
                {"preparing", get_mission_segment() }
            };
            return missions;
        }
        public static List<List<int>> get_tower_item_obtained()
        {
            int total_stars = next_int(4);
            int total_stages = next_int(4);
            List<List<int>> data = new List<List<int>>();
            for (int i = 0; i < total_stars; i++)
            {
                List<int> star_data = new List<int>();
                for (int j = 0; j < total_stages; j++)
                {
                    star_data.Add(next_int(1));
                }
            }
            return data;
        }
        public static List<Dictionary<string, int>> get_data_after_tower()
        {
            List<Dictionary<string, int>> data = new List<Dictionary<string, int>>();
            var gv_66 = next_int_len(4); //0x42
            data.Add(gv_66);

            data.Add(next_int_len(4 * 2));
            data.Add(next_int_len(1 * 3));
            data.Add(next_int_len(4 * 3));
            data.Add(next_int_len(1 * 3));
            data.Add(next_int_len(1));
            data.Add(next_int_len(8));

            var val_54 = next_int_len(4);
            data.Add(val_54);

            var val_61 = next_int_len(4);
            data.Add(val_61);

            for (int i = 0; i < val_54["Value"]; i++)
            {
                for (int j = 0; j < val_61["Value"]; j++)
                {
                    var val_22 = next_int_len(4);
                    data.Add(val_22);
                }
            }

            val_54 = next_int_len(4);
            data.Add(val_54);

            val_61 = next_int_len(4);
            data.Add(val_61);

            for (int i = 0; i < val_54["Value"]; i++)
            {
                for (int j = 0; j < val_61["Value"]; j++)
                {
                    var val_22 = next_int_len(4);
                    data.Add(val_22);
                }
            }

            val_54 = next_int_len(4);
            data.Add(val_54);

            val_61 = next_int_len(4);
            data.Add(val_61);

            var val_57 = next_int_len(4);
            data.Add(val_57);

            for (int i = 0; i < val_54["Value"]; i++)
            {
                for (int j = 0; j < val_61["Value"]; j++)
                {
                    for (int k = 0; k < val_57["Value"]; k++)
                    {
                        var val_22 = next_int_len(4);
                        data.Add(val_22);
                    }
                }
            }

            val_54 = next_int_len(4);
            data.Add(val_54);

            val_61 = next_int_len(4);
            data.Add(val_61);

            for (int i = 0; i < val_54["Value"]; i++)
            {
                for (int j = 0; j < val_61["Value"]; j++)
                {
                    var val_22 = next_int_len(4);
                    data.Add(val_22);
                }
            }

            val_54 = next_int_len(4);
            data.Add(val_54);

            for (int i = 0; i < val_54["Value"]; i++)
            {
                var val_22 = next_int_len(4);
                data.Add(val_22);
            }

            return data;
        }
        public static Dictionary<dynamic, dynamic> get_dict(Type key_type, Type value_type, int? length = null)
        {
            if (length == null) length = next_int(4);
            Dictionary<dynamic, dynamic> data = new Dictionary<dynamic, dynamic>();
            int key;
            for (int i = 0; i < length; i++)
            {
                if (key_type == typeof(int))
                {
                    key = next_int(4);
                }
                else throw new Exception("Invalid key type");
                if (value_type == typeof(int)) data[key] = next_int(4);
                else if (value_type == typeof(string)) data[key] = get_utf8_string();
                else if (value_type == typeof(bool)) data[key] = next_int(1) == 1;
                else throw new Exception("Invalid value type");
            }
            return data;
        }
        public static List<Dictionary<string, int>> get_data_after_challenge()
        {
            List<Dictionary<string, int>> data = new List<Dictionary<string, int>>();

            var val_22 = next_int_len(4);
            data.Add(val_22);

            var gv_69 = next_int_len(4); //0x45
            data.Add(gv_69);

            var val_54 = next_int_len(4);
            data.Add(val_54);

            var val_118 = next_int_len(4);
            data.Add(val_118);

            for (int i = 0; i < val_54["Value"]; i++)
            {
                var val_15 = next_int_len(1);
                data.Add(val_15);

                val_118 = next_int_len(4);
                data.Add(val_118);
            }

            val_54 = next_int_len(4);
            data.Add(val_54);

            for (int i = 0; i < val_118["Value"]; i++)
            {
                var val_65 = next_int_len(8);
                data.Add(val_65);

                val_54 = next_int_len(4);
                data.Add(val_54);
            }
            return data;
        }
        public static Dictionary<string, dynamic> get_uncanny_current()
        {
            int total_subchapters = next_int(4);
            int stages_per_subchapter = next_int(4);
            int stars = next_int(4);
            List<int> clear_progress;
            if (total_subchapters < 1)
            {
                next_int(4);
                throw new Exception("Invalid total subchapters");
            }
            else
            {
                clear_progress = get_length_data(4, 4, total_subchapters * stars);
            }
            List<List<int>> clear_progress_ = (List<List<int>>)chunks(clear_progress, stars);

            return new Dictionary<string, dynamic>()
{
    {"Clear", clear_progress_ },
    {"total", total_subchapters },
    {"stages", stages_per_subchapter },
    {"stars", stars }
};
        }
        public static Dictionary<string, dynamic> get_uncanny_progress(Dictionary<string, dynamic> lengths)
        {
            var total = (int)lengths["total"];
            var stars = (int)lengths["stars"];
            var stages = (int)lengths["stages"];

            var clear_progress = get_length_data(4, 4, total * stars);
            var clear_progress_ = (List<List<int>>)chunks(clear_progress, stars);

            var clear_amount = get_length_data(4, 4, total * stages * stars);
            var unlock_next = get_length_data(4, 4, total * stars);

            var clear_amount_ = (List<List<int>>)chunks(clear_amount, stages * stars);
            var unlock_next_ = (List<List<int>>)chunks(unlock_next, stars);

            List<List<List<int>>> clear_amount_sep = new List<List<List<int>>>();

            foreach (var clear_amount_val in clear_amount_)
            {
                List<List<int>> sub_chapter_clears = new List<List<int>>();
                for (int j = 0; j < stars; j++)
                {
                    var sub_chapter = clear_amount_val.Where((n, index) => index % stars == j && index > j).ToList();
                    sub_chapter_clears.Add(sub_chapter);
                }
                clear_amount_sep.Add(sub_chapter_clears);
            }
            Dictionary<string, dynamic> value = new Dictionary<string, dynamic>()
{
    { "clear_progress", clear_progress_ },
    { "clear_amount", clear_amount_ },
    { "unlock_next", unlock_next_ },
};
            return new Dictionary<string, dynamic>()
{
    {"Value", value },
    {"Lengths", lengths }
};
        }
        public static Dictionary<string, dynamic> get_data_after_uncanny()
        {
            var lengths = get_uncanny_current();
            return new Dictionary<string, dynamic>()
            {
                {"current", lengths },
                {"progress" , get_uncanny_progress(lengths) }
            };
        }
        public static Dictionary<string, dynamic> get_gold_pass_data()
        {
            // Get gold pass related data
            Dictionary<string, dynamic> data = new Dictionary<string, dynamic>();
            data["officer_id"] = next_int_len(4);
            data["renewal_times"] = next_int_len(4);
            data["start_date"] = get_double();
            data["expiry_date"] = get_double();
            data["unknown_2"] = get_length_doubles(length: 2);
            data["start_date_2"] = get_double();
            data["expiry_date_2"] = get_double();
            data["unknown_3"] = get_double();
            data["flag_2"] = next_int_len(4);
            data["expiry_date_3"] = get_double();

            int number_of_rewards = next_int(4);
            Dictionary<int, int> claimed_rewards = new Dictionary<int, int>();
            for (int i = 0; i < number_of_rewards; i++)
            {
                int item_id = next_int(4);
                int amount = next_int(4);
                claimed_rewards[item_id] = amount;
            }
            data["claimed_rewards"] = claimed_rewards;
            data["unknown_4"] = next_int_len(8);
            data["unknown_5"] = next_int_len(1);
            data["unknown_6"] = next_int_len(1);

            return data;
        }
        public static Dictionary<int, List<Dictionary<string, int>>> get_talent_data()
        {
            int total_cats = next_int(4);

            Dictionary<int, List<Dictionary<string, int>>> talents = new Dictionary<int, List<Dictionary<string, int>>>();
            for (int i = 0; i < total_cats; i++)
            {
                int cat_id = next_int(4);
                List<Dictionary<string, int>> cat_data = new List<Dictionary<string, int>>();

                int number_of_talents = next_int(4);

                for (int j = 0; j < number_of_talents; j++)
                {
                    int talent_id = next_int(4);
                    int talent_level = next_int(4);
                    Dictionary<string, int> talent = new Dictionary<string, int>()
                    {
                        { "id", talent_id },
                        { "level", talent_level }
                    };
                    cat_data.Add(talent);
                }
                talents[cat_id] = cat_data;
            }
            return talents;
        }
        public static Dictionary<string, dynamic> get_legend_quest_current()
        {
            int total_subchapters = next_int(1);
            int stages_per_subchapter = next_int(1);
            int stars = next_int(1);

            var clear_progress = get_length_data(4, 1, total_subchapters * stars);
            var clear_progress_ = (List<List<int>>)chunks(clear_progress, stars);

            return new Dictionary<string, dynamic>
            {
                {"Clear", clear_progress_ },
                {"total", total_subchapters },
                {"stages", stages_per_subchapter },
                {"stars", stars }
            };
        }
        public static Dictionary<string, dynamic> get_legend_quest_progress(Dictionary<string, dynamic> lengths)
        {
            int total = lengths["total"];
            int stars = lengths["stars"];
            int stages = lengths["stages"];

            var clear_progress = get_length_data(4, 1, total * stars);
            var clear_progress_ = (List<List<int>>)chunks(clear_progress, stars);
            var clear_amount = get_length_data(4, 2, total * stars * stages);
            var tries = get_length_data(4, 2, total * stars * stages);

            var unlock_next = get_length_data(4, 1, total * stars);
            var unlock_next_ = (List<List<int>>)chunks(unlock_next, stars);

            var clear_amount_ = (List<List<int>>)chunks(clear_amount, stages * stars);
            var tries_ = (List<List<int>>)chunks(tries, stages * stars);

            List<List<List<int>>> clear_amount_sep = new List<List<List<int>>>();
            List<List<List<int>>> stage_ids_sep = new List<List<List<int>>>();

            foreach (var clear_amount_val in clear_amount_)
            {
                List<List<int>> sub_chapter_clears = new List<List<int>>();
                for (int j = 0; j < stars; j++)
                {
                    var sub_clears = clear_amount_val.Where((n, index) => index % stars == j && index > j).ToList();
                    sub_chapter_clears.Add(sub_clears);
                }
                clear_amount_sep.Add(sub_chapter_clears);
            }

            foreach (var stage_id_val in tries_)
            {
                List<List<int>> sub_chapter_ids = new List<List<int>>();
                for (int j = 0; j < stars; j++)
                {
                    var sub_clears = stage_id_val.Where((n, index) => index % stars == j && index>j).ToList();
                    sub_chapter_ids.Add(sub_clears);
                }
                stage_ids_sep.Add(sub_chapter_ids);
            }
            Dictionary<string, dynamic> data = new Dictionary<string, dynamic>()
            {
                {"clear_progress", clear_progress_ },
                {"clear_amount", clear_amount_sep },
                {"tries", stage_ids_sep },
                {"unlock_next", unlock_next_ }
            };
            return new Dictionary<string, dynamic>()
            {
                {"Value", data},
                {"Lengths", lengths }
            };
        }
        public static List<Dictionary<string, int>> get_data_after_leadership()
        {
            List<Dictionary<string, int>> data = new List<Dictionary<string, int>>();

            data.Add(next_int_len(2));
            data.Add(next_int_len(1));
            data.Add(next_int_len(4)); //80600

            var val_54 = next_int_len(4);
            data.Add(val_54);

            if (val_54["Value"] > 0)
            {
                var val_118 = next_int_len(4);
                data.Add(val_118);

                var val_55 = next_int_len(4);
                data.Add(val_55);

                for (int i = 0; i < val_55["Value"]; i++)
                {
                    var val_61 = next_int_len(4);
                    data.Add(val_61);
                }

                for (int i = 0; i < val_54["Value"] - 1; i++)
                {
                    var val_61 = next_int_len(4);
                    data.Add(val_61);

                    val_61 = next_int_len(4);
                    data.Add(val_61);
                }
            }
            return data;
        }
        public static List<Dictionary<string, int>> get_data_after_after_leadership(bool dst)
        {
            List<Dictionary<string, int>> data = new List<Dictionary<string, int>>();
            data.Add(next_int_len(4));
            if (dst == false) data.Add(next_int_len(5));
            if (dst == true) data.Add(next_int_len(12));
            else data.Add(next_int_len(7));
            return data;
        }
        public static Dictionary<string, dynamic> get_medals()
        {
            var medal_data_1 = get_length_data(2, 2);

            int total_medals = next_int(2);
            Dictionary<dynamic, dynamic> medals = new Dictionary<dynamic, dynamic>();

            for (int i = 0; i < total_medals; i++)
            {
                int medal_id = next_int(2);
                int medal_flag = next_int(1);
                medals[medal_id] = medal_flag;
            }
            return new Dictionary<string, dynamic>
            {
                {"medal_data_1", medal_data_1 },
                {"medal_data_2", medals }
            };
        }
        public static List<Dictionary<string, int>> get_data_after_medals()
        {
            List<Dictionary<string, int>> data = new List<Dictionary<string, int>>();
            data.Add(next_int_len(1));

            var val_2 = next_int_len(2);
            data.Add(val_2);

            var val_3 = next_int_len(2);
            data.Add(val_3);

            for (int i = 0; i < val_2["Value"]; i++)
            {
                var val_1 = next_int_len(1);
                data.Add(val_1);

                val_3 = next_int_len(2);
                data.Add(val_3);
            }
            val_2 = next_int_len(2);
            data.Add(val_2);

            var val_6c = val_3;
            for (int i = 0; i < val_6c["Value"]; i++)
            {
                val_2 = next_int_len(2);
                data.Add(val_2);
                for (int j = 0; j < val_2["Value"]; j++)
                {
                    val_3 = next_int_len(2);
                    data.Add(val_3);

                    var val_4 = next_int_len(2);
                    data.Add(val_4);
                }
                val_2 = next_int_len(2);
                data.Add(val_2);
            }

            var val_7c = val_2;
            for (int i = 0; i < val_7c["Value"]; i++)
            {
                val_2 = next_int_len(2);
                data.Add(val_2);

                var val_12 = next_int_len(2);
                data.Add(val_12);
            }

            data.Add(next_int_len(4)); //90000

            data.Add(next_int_len(4));
            data.Add(next_int_len(4));
            data.Add(next_int_len(8));
            data.Add(next_int_len(4)); //9010

            var val_18 = next_int_len(2);
            data.Add(val_18);
            for (int i = 0; i < val_18["Value"]; i++)
            {
                data.Add(next_int_len(4));
                data.Add(next_int_len(4));
                data.Add(next_int_len(2));
                data.Add(next_int_len(4));
                data.Add(next_int_len(4));
                data.Add(next_int_len(4));
                data.Add(next_int_len(2));
            }

            val_18 = next_int_len(2);
            data.Add(val_18);
            for (int i = 0; i < val_18["Value"]; i++)
            {
                var val_32 = next_int_len(4);
                data.Add(val_32);

                var val_48 = next_int_len(8);
                data.Add(val_48);
            }
            return data;
        }
        public static Dictionary<string, dynamic> get_gauntlet_current()
        {
            int total_subchapters = next_int(2);
            int stages_per_subchapter = next_int(1);
            int stars = next_int(1);

            var clear_progress = get_length_data(4, 1, total_subchapters * stars);
            List<List<int>> clear_progress_ = (List<List<int>>)chunks(clear_progress, stars);

            return new Dictionary<string, dynamic>
            {
                {"Clear", clear_progress_ },
                {"total", total_subchapters },
                {"stages", stages_per_subchapter },
                {"stars", stars }
            };
        }
        public static Dictionary<string, dynamic> get_gauntlet_progress(Dictionary<string, dynamic> lenghts, bool? unlock = true)
        {
            int total = lenghts["total"];
            int stars = lenghts["stars"];
            int stages = lenghts["stages"];

            var clear_progress = get_length_data(4, 1, total * stars);
            List<List<int>> clear_progress_ = (List<List<int>>)chunks(clear_progress, stars);

            List<List<int>> unlock_next_ = new List<List<int>>();

            var clear_amount = get_length_data(4, 2, total * stages * stars);
            List<int> unlock_next = new List<int>();
            if (unlock == true)
            {
                unlock_next = get_length_data(4, 1, total * stars);
                unlock_next_ = (List<List<int>>)chunks(unlock_next, stars);
            }

            List<List<int>> clear_amount_ = (List<List<int>>)chunks(clear_amount, stages * stars);

            List<List<List<int>>> clear_amount_sep = new List<List<List<int>>>();

            foreach (var clear_amount_val in clear_amount_)
            {
                List<List<int>> sub_chapter_clears = new List<List<int>>();
                for (int j = 0; j < stars; j++)
                {
                    var sub_clears = clear_amount_val.Where((n, index) => index % stars == j && index > j).ToList();
                    sub_chapter_clears.Add(sub_clears);
                }
                clear_amount_sep.Add(sub_chapter_clears);
            }
            Dictionary<string, dynamic> value = new Dictionary<string, dynamic>()
            {
                { "clear_progress", clear_progress_ },
                { "clear_amount", clear_amount_sep },
                { "unlock_next", unlock_next_ }
            };
            return new Dictionary<string, dynamic>()
            {
                {"Value", value },
                {"Lengths", lenghts }
            };
        }
        public static Dictionary<string,dynamic> get_enigma_stages()
        {
            Dictionary<string,dynamic> enigma_data = new Dictionary<string,dynamic>();
            enigma_data["energy_since_1"] = next_int(4);
            enigma_data["energy_since_2"] = next_int(4);
            enigma_data["enigma_level"] = next_int(1);
            enigma_data["unknown_2"] = next_int(1);
            enigma_data["unknown_3"] = next_int(1);

            int total_stages = next_int(1);
            List<Dictionary<string,dynamic>> stages = new List<Dictionary<string,dynamic>>();
            for (int i = 0; i < total_stages; i++)
            {
                Dictionary<string, dynamic> data = new Dictionary<string, dynamic>();
                data["level"] = next_int(4); //0 = inferior, 1 = normal, 2 = superior
                data["stage_id"] = next_int(4);
                data["decoding_status"] = next_int(1); //0 = not decoded, 1 = decoded, 2 = revealed
                data["start_time"] = get_double();
                stages.Add(data);
            }
            enigma_data["stages"] = stages;
            return enigma_data;
        }
        public static (Dictionary<string,dynamic>,List<Dictionary<string,int>>) get_cleared_slots()
        {
            int total_slots = next_int(2);
            int index = next_int(2);

            List<ClearedSlots.Slot> slots = new List<ClearedSlots.Slot>();

            for (int i=0; i<total_slots; i++)
            {
                List<ClearedSlots.Slot.Cat> cats = new List<ClearedSlots.Slot.Cat>();
                for (int j=0; j<10;  j++)
                {
                    int cat_id = next_int(2);
                    int cat_form = next_int(1);
                    var cat_data = new ClearedSlots.Slot.Cat(cat_id,cat_form);
                    cats.Add(cat_data);
                }
                int separator = next_int(3);
                var slot = new ClearedSlots.Slot(cats,index,separator);
                index = next_int(2);
                slots.Add(slot);
            }

            var cleared_slot_data = new List<ClearedSlots.StageSlot>();
            int index_2 = next_int(2);
            for (int i=0; i<index; i++)
            {
                int total_stages = next_int(2);
                var stages = new List<ClearedSlots.StageSlot.Stage>();
                for (int j=0; j<total_stages; j++)
                {
                    int stage_id = next_int(4);
                    var stage = new ClearedSlots.StageSlot.Stage(stage_id);
                    stages.Add(stage);
                }
                var stages_data = new ClearedSlots.StageSlot(index_2, stages);
                index_2 = next_int(2);
                cleared_slot_data.Add(stages_data);
            }

            var data_2 = new List<Dictionary<string, int>>();
            var temp = new Dictionary<string, int>()
            {
                {"Value", index_2},
                {"Length", 2}
            };
            data_2.Add(temp);
            for (int i=0; i<index_2; i++)
            {
                var val_18 = next_int_len(2);
                data_2.Add(val_18);

                var val_4 = next_int_len(1);
                data_2.Add(val_4);
            }
            data_2.Add(next_int_len(4)); //90400

            var cleared_slots = new ClearedSlots(slots, cleared_slot_data, index);

            return (cleared_slots.to_dict(), data_2);
        }
        public static List<Dictionary<string,int>> data_after_after_gauntlets()
        {
            List<Dictionary<string, int>> data = new List<Dictionary<string, int>>();
            data.Add(next_int_len(1));
            data.Add(next_int_len(8 * 2));
            data.Add(next_int_len(4));
            data.Add(next_int_len(1 * 2));
            data.Add(next_int_len(8 * 2));
            data.Add(next_int_len(4));  // 90500
            return data;
        }
        public static Dictionary<int,int> get_talent_orbs(Dictionary<string,dynamic> game_version)
        {
            Dictionary<int,int> talent_orb_data = new Dictionary<int, int>();

            int total_orbs = next_int(2);
            int amount;
            for (int i=0; i<total_orbs; i++)
            {
                int orb_id = next_int(2);
                if (game_version["Value"] < 110400) amount = next_int(1);
                else amount = next_int(2);
                talent_orb_data[orb_id] = amount;
            }
            return talent_orb_data;
        }
        public static List<Dictionary<string,int>> get_data_after_orbs()
        {
            List<Dictionary<string, int>> data = new List<Dictionary<string, int>>();
            var val_31 = next_int_len(2);
            data.Add(val_31);
            for (int i=0; i < val_31["Value"]; i++)
            {
                var val_18 = next_int_len(2);
                data.Add(val_18);

                var val_5 = next_int_len(1);
                data.Add(val_5);

                for (int j=0; j < val_5["Value"]; j++)
                {
                    var val_6 = next_int_len(1);
                    data.Add(val_6);

                    val_18 = next_int_len(2);
                    data.Add(val_18);
                }
            }
            data.Add(next_int_len(1));
            data.Add(next_int_len(4)); //90700

            var length = next_int_len(2);
            data.Add(length);
            for (int i=0; i < length["Value"]; i++)
            {
                data.Add(next_int_len(4));
            }

            data.Add(next_int_len(1 * 10));
            data.Add(next_int_len(4)); //90800

            data.Add(next_int_len(1));
            return data;
        }
        public static Dictionary<string,dynamic> get_cat_shrine_data()
        {
            var stamp_1 = get_double();
            var stamp_2 = get_double();
            int shrine_gone = next_int(1);
            List<int> flags = get_length_data(1, 1);
            int xp_offering = next_int(4);
            return new Dictionary<string, dynamic>()
            {
                { "flag", flags },
                { "xp_offering", xp_offering },
                { "shrine_gone", shrine_gone },
                { "stamp_1", stamp_1 },
                { "stamp_2", stamp_2 },
            };
        }
        public static List<string> get_slot_names(Dictionary<string,dynamic> save_stats)
        {
            int total_slots = save_stats["slots"].Count;
            if (save_stats["game_version"]["Value"] >= 110600) total_slots = next_int(1);
            List<string> names = new List<string>();
            for (int i=0; i<total_slots; i++)
            {
                names.Add(get_utf8_string());
            }
            return names;
        }
        public static Dictionary<string,dynamic> exit_parser(Dictionary<string,dynamic> save_stats)
        {
            save_stats["hash"] = get_utf8_string(32);
            return save_stats;
        }
        public static Dictionary<string,dynamic> check_gv(Dictionary<string,dynamic> save_stats,int game_version)
        {
            if (save_stats["game_version"]["Value"] < game_version)
            {
                save_stats = exit_parser(save_stats);
                save_stats["exit"] = true;
                save_stats["extra_data"] = next_int_len(0);
            }
            else save_stats["exit"] = false;
            return save_stats;
        }
        public static List<Dictionary<string,int>> get_data_near_end()
        {
            List<Dictionary<string, int>> data = new List<Dictionary<string, int>>();
            var val_5 = next_int_len(1);
            data.Add(val_5);
            if (0 < val_5["Value"])
            {
                var val_33 = next_int_len(4);
                data.Add(val_33);
                if (val_5["Value"] != 1)
                {
                    val_33 = next_int_len(4);
                    data.Add(val_33);
                    if (val_5["Value"] != 2)
                    {
                        var va_32 = val_5["Value"] + 2;
                        for (int i=0; i<va_32; i++)
                        {
                            data.Add(next_int_len(4));
                        }
                    }
                }
            }

            data.Add(next_int_len(1));
            data.Add(next_int_len(4)); //1000400
            data.Add(next_int_len(8));
            return data;
        }
        public static List<Dictionary<string, int>> get_data_near_end_after_shards()
        {
            List<Dictionary<string, int>> data = new List<Dictionary<string, int>>();
            data.Add(next_int_len(1));
            data.Add(next_int_len(4)); //100600

            var val_2 = next_int_len(2);
            data.Add(val_2);

            var val_3 = next_int_len(2);
            data.Add(val_3);

            for (int i=0; i < val_2["Value"]; i++)
            {
                var val_1 = next_int_len(1);
                data.Add(val_1);

                val_3 = next_int_len(2);
                data.Add(val_3);
            }
            var val_6c = val_3;

            val_2 = next_int_len(2);
            data.Add(val_2);

            for (int i=0; i < val_6c["Value"]; i++)
            {
                val_2 = next_int_len(2);
                data.Add(val_2);

                for (int j=0; j < val_2["Value"]; j++)
                {
                    val_3 = next_int_len(2);
                    data.Add(val_3);

                    var val_4 = next_int_len(2);
                    data.Add(val_4);
                }
                val_2 = next_int_len(2);
                data.Add(val_2);
            }
            var val_7c = val_2;
            for (int i=0;i < val_7c["Value"]; i++)
            {
                val_2 = next_int_len(2);
                data.Add(val_2);

                var val_12 = next_int_len(4);
                data.Add(val_12);
            }
            return data;
        }
        public static Dictionary<string,dynamic> get_aku()
        {
            int total = next_int(2);
            int stages = next_int(1);
            int stars = next_int(1);
            Dictionary<string, dynamic> data = new Dictionary<string, dynamic>()
            {
                {"total", total },
                {"stages", stages },
                {"stars", stars }
            };
            return get_gauntlet_progress(data, false);
        }
        public static List<Dictionary<string,int>> get_data_after_aku()
        {
            List<Dictionary<string, int>> data_1 = new List<Dictionary<string, int>>();

            var val_6 = next_int_len(2);
            data_1.Add(val_6);

            var val_7 = next_int_len(2);
            data_1.Add(val_7);

            for (int i=0; i < val_6["Value"]; i++)
            {
                val_7 = next_int_len(2);
                data_1.Add(val_7);

                for(int j=0; j < val_7["Value"]; j++)
                {
                    data_1.Add(next_int_len(2));
                }
                val_7 = next_int_len(2);
                data_1.Add(val_7);
            }
            var val_4c = val_7;
            for (int i=0; i < val_4c["Value"]; i++)
            {
                data_1.Add(next_int_len(2));
                data_1.Add(next_int_len(8));
            }

            var val_5 = next_int_len(2);
            data_1.Add(val_5);

            for (int i=0;i < val_5["Value"]; i++)
            {
                data_1.Add(next_int_len(2));
                data_1.Add(next_int_len(8));
            }

            data_1.Add(next_int_len(1));
            return data_1;
        }
        public static List<Dictionary<string,int>> get_data_near_end_after_aku()
        {
            List<Dictionary<string, int>> data_2 = new List<Dictionary<string, int>>();
            var val_4 = next_int_len(2);
            data_2.Add(val_4);

            for (int i=0; i < val_4["Value"]; i++)
            {
                data_2.Add(next_int_len(4));
                data_2.Add(next_int_len(1));
                data_2.Add(next_int_len(1));
            }
            return data_2;
        }
        public static List<Dictionary<string,int>> get_110700_data()
        {
            List<Dictionary<string, int>> data = new List<Dictionary<string, int>>();
            var i_var_32 = next_int_len(4);
            data.Add(i_var_32);

            for (int i=0; i < i_var_32["Value"]; i++)
            {
                var pi_var_33 = next_int_len(4);
                data.Add(pi_var_33);

                var f_var_54 = next_int_len(8);
                data.Add(f_var_54);

                f_var_54 = next_int_len(8);
                data.Add(f_var_54);
            }
            return data;
        }
        public static List<Dictionary<string,int>> get_110800_data()
        {
            List<Dictionary<string, int>> data = new List<Dictionary<string, int>>();

            var u_var_38 = next_int_len(1);
            data.Add(u_var_38);

            u_var_38 = next_int_len(1);
            data.Add(u_var_38);

            return data;
        }
        public static List<Dictionary<string,int>> get_110800_data_2()
        {
            List<Dictionary<string, int>> data = new List<Dictionary<string, int>>();

            var u_var_38 = next_int_len(1);
            data.Add(u_var_38);

            u_var_38 = next_int_len(1);
            data.Add(u_var_38);

            return data;
        }
        public static List<Dictionary<string,int>> get_110900_data()
        {
            List<Dictionary<string, int>> data = new List<Dictionary<string, int>>();

            data.Add(next_int_len(4));
            data.Add(next_int_len(2));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));

            var ivar_14 = next_int_len(1);
            data.Add(ivar_14);

            for (int i = 0; i < ivar_14["Value"]; i++)
            {
                data.Add(next_int_len(2));
            }

            var svar6 = next_int_len(2);
            data.Add(svar6);

            for (int i = 0;i < svar6["Value"]; i++)
            {
                data.Add(next_int_len(2));
            }

            svar6 = next_int_len(2);
            data.Add(svar6);

            for (int i = 0; i < svar6["Value"]; i++)
            {
                data.Add(next_int_len(2));
            }

            data.Add(next_int_len(4));
            data.Add(next_int_len(4));
            data.Add(next_int_len(4));
            data.Add(next_int_len(2));
            data.Add(next_int_len(2));
            data.Add(next_int_len(2));
            data.Add(next_int_len(2));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            svar6 = next_int_len(2);
            data.Add(svar6);

            for (int i = 0;i < svar6["Value"]; i++)
            {
                data.Add(next_int_len(2));
            }

            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));
            data.Add(next_int_len(1));

            var cvar4 = next_int_len(1);
            data.Add(cvar4);
            if (0 < cvar4["Value"])
            {
                data.Add(next_int_len(2));
                if (cvar4["Value"] != 1)
                {
                    data.Add(next_int_len(2));
                    if (cvar4["Value"] != 2)
                    {
                        data.Add(next_int_len(2));
                        if (cvar4["Value"] != 3)
                        {
                            data.Add(next_int_len(2));
                            if (cvar4["Value"] != 4)
                            {
                                var ivar_32 = cvar4["Value"] + 4;
                                for (int i = 0; i<ivar_32; i++)
                                {
                                    data.Add(next_int_len(2));
                                }
                            }
                        }
                    }
                }
            }
            return data;
        }
        public static List<Dictionary<string, dynamic>> get_zero_legends()
        {
            int total_chapters = next_int(2);
            List<Dictionary<string, dynamic>> chapters = new List<Dictionary<string, dynamic>>();
            for (int i = 0; i < total_chapters; i++)
            {
                int unknown_1 = next_int(1);
                int total_stars = next_int(1);
                List<Dictionary<string, dynamic>> stars = new List<Dictionary<string, dynamic>>();
                for (int j = 0; j < total_stars; j++)
                {
                    int selected_stage = next_int(1);
                    int stages_cleared = next_int(1);
                    int unlock_next = next_int(1);
                    int total_stages = next_int(2);
                    List<dynamic> stages = new List<dynamic>();
                    for (int k = 0; k < total_stages; k++)
                    {
                        int clear_amount = next_int(2);
                        stages.Add(clear_amount);
                    }
                    Dictionary<string, dynamic> temp = new Dictionary<string, dynamic>()
                    {
                        { "selected_stage" , selected_stage },
                        { "stages_cleared", stages_cleared},
                        { "unlock_next", unlock_next},
                        { "stages", stages},
                    };
                    stars.Add(temp);
                }
                Dictionary<string, dynamic> temp_2 = new Dictionary<string, dynamic>()
                    {
                        { "unknown_1" , unknown_1 },
                        { "stars", stars},
                    };
                chapters.Add(temp_2);
            }
            return chapters;
        }
        public static List<Dictionary<string, int>> get_120100_data()
        {
            List<Dictionary<string, int>> data = new List<Dictionary<string, int>>();
            var svar19 = next_int_len(2);
            data.Add(svar19);
            for (int i = 0; i < svar19["Value"]; i++)
            {
                data.Add(next_int_len(2));
            }

            return data;
        }
        public static List<Dictionary<string, int>> get_120200_data()
        {
            List<Dictionary<string, int>> data = new List<Dictionary<string, int>>();
            data.Add(next_int_len(1));
            data.Add(next_int_len(2));
            var cvar4 = next_int_len(1);
            data.Add(cvar4);
            for (int i=0; i < cvar4["Value"]; i++)
            {
                data.Add(next_int_len(2));
                data.Add(next_int_len(2));
            }
            return data;
        }
        public static int get_game_version(byte[] save_data)
        {
            return convert_little(save_data.Take(4).ToArray());
        }
        public static Dictionary<string, dynamic> parse_save(byte[] save_data, string country_code, bool? dst = null)
        {
            if (country_code == "ja" || country_code == "")
            {
                country_code = "jp";
            }

            set_address(0);

            save_data_g = save_data;
            var save_stats = new Dictionary<string,dynamic>();

            save_stats["editor_version"] = "0.0.1";

            save_stats["game_version"] = next_int_len(4);
            save_stats["version"] = country_code;

            save_stats["unknown_1"] = next_int_len(1);

            save_stats["mute_music"] = next_int_len(1);
            save_stats["mute_sound_effects"] = next_int_len(1);

            save_stats["cat_food"] = next_int_len(4);
            save_stats["current_energy"] = next_int_len(4);

            int old_address = address;
            int new_address = find_date();
            set_address(old_address);
            int extra = new_address - old_address;
            save_stats["extra_time_data"] = next_int_len(extra);

            if (dst == null)  dst = get_dst(save_data, address + 118);
            save_stats["dst"] = dst;
            if ((save_stats["version"] == "jp" && dst) || (save_stats["version"] != "jp" && !dst))
            {
                MessageBox.Show("Warning: DST detected is not correct for this save version\nthis may cause issues with save parsing.");
               //throw new Exception();
            }
            Dictionary<string, dynamic> data = get_time_data_skip(save_stats["dst"]);

            save_stats["time"] = data["time"];
            save_stats["dst_val"] = data["dst"];
            save_stats["time_stamp"] = data["time_stamp"];
            save_stats["duplicate_time"] = data["duplicate"];


            save_stats["unknown_flags_1"] = get_length_data(length:3);
            save_stats["upgrade_state"] = next_int_len(4);
            save_stats["xp"] = next_int_len(4);

            save_stats["tutorial_cleared"] = next_int_len(4);
            save_stats["unknown_flags_2"] = get_length_data(length: 12);
            save_stats["unknown_flag_1"] = next_int_len(1);
            save_stats["slots"] = get_equip_slots();

            save_stats["cat_stamp_current"] = next_int_len(4);

            save_stats["cat_stamp_collected"] = get_length_data(length:30);
            save_stats["unknown_2"] = next_int_len(4);
            save_stats["daily_reward_flag"] = next_int_len(4);
            save_stats["unknown_116"] = get_length_data(length:10);

            save_stats["story_chapters"] = get_main_story_levels();
            save_stats["treasures"] = get_treasures();
            try
            {
                save_stats["enemy_guide"] = get_length_data();
            }
                
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                return parse_save(save_data, country_code, !dst);
            }
            if (save_stats["enemy_guide"].Count == 0)
            {
                return parse_save(save_data, country_code, !dst);
            }
            save_stats["cats"] = get_length_data();
            save_stats["cat_upgrades"] = get_cat_upgrades();
            save_stats["current_forms"] = get_length_data();

            save_stats["blue_upgrades"] = get_blue_upgrades();

            save_stats["menu_unlocks"] = get_length_data();
            save_stats["new_dialogs_1"] = get_length_data();

            save_stats["battle_items"] = get_length_data(4, 4, 6);
            save_stats["new_dialogs_2"] = get_length_data();
            save_stats["unknown_6"] = next_int_len(4);
            save_stats["unknown_7"] = get_length_data(length:21);

            save_stats["lock_item"] = next_int_len(1);
            save_stats["locked_items"] = get_length_data(1, 1, 6);
            save_stats["second_time"] = get_time_data(save_stats["dst"]);

            save_stats["unknown_8"] = get_length_data(length:50);
            save_stats["third_time"] = get_time_data(save_stats["dst"]);

            save_stats["unknown_9"] = next_int_len(6 * 4);

            save_stats["thirty2_code"] = get_utf8_string();
            save_stats["unknown_10"] = load_bonus_hash();
            save_stats["unknown_11"] = get_length_data(length:4);
            save_stats["normal_tickets"] = next_int_len(4);
            save_stats["rare_tickets"] = next_int_len(4);
            save_stats["gatya_seen_cats"] = get_length_data();
            save_stats["unknown_12"] = get_length_data(length:10);
            int length = next_int(2);
            bool cat_storage_len = true;
            if (length != 128)
            {
                skip(-2);
                cat_storage_len = false;
                length = 100;
            }

            List<int> cat_storage_id = get_length_data(2, 4, length);
            List<int> cat_storage_type = get_length_data(2, 4, length);
            save_stats["cat_storage"] = new Dictionary<string, dynamic>()
{
    { "ids", cat_storage_id },
    { "types", cat_storage_type },
    { "len", cat_storage_len }
};
            Dictionary<string,dynamic> current_sel = get_event_stages_current();
            save_stats["event_current"] = current_sel;

            save_stats["event_stages"] = get_event_stages(current_sel);

            save_stats["unknown_15"] = get_length_data(length:38);
            save_stats["unit_drops"] = get_length_data();
            save_stats["rare_gacha_seed"] = next_int_len(4);
            save_stats["unknown_17"] = next_int_len(12);
            save_stats["unknown_18"] = next_int_len(4);

            save_stats["fourth_time"] = get_time_data(save_stats["dst"]);
            save_stats["unknown_105"] = get_length_data(length = 5);
            save_stats["unknown_107"] = get_length_data(separator:1, length:3);

            if (save_stats["dst"] == true) save_stats["unknown_110"] = get_utf8_string();
            else save_stats["unknown_110"] = "";

            int total_strs = next_int(4);
            List<string> unknown_108 = new List<string>();
            for (int i=0; i<total_strs; i++)
            {
                unknown_108.Add(get_utf8_string());
            }
            save_stats["unknown_108"] = unknown_108;

            if (save_stats["dst"] == true)
            {
                save_stats["time_stamos"] = get_length_doubles(length: 3);
                int length_ = next_int(4);
                List<string> strs = new List<string>();
                for(int i=0; i<length_; i++)
                {
                    strs.Add(get_utf8_string());
                }
                save_stats["unknown_112"] = strs;
                save_stats["energy_notice"] = next_int_len(1);
                save_stats["game_version_2"] = next_int_len(4);
            }
            else
            {
                save_stats["time_stamps"] = new List<int> { 0, 0, 0 };
                save_stats["unknown_112"] = new List<string>();
                save_stats["energy_notice"] = generate_empty_len(1);
                save_stats["game_version_2"] = generate_empty_len(4);
            }

            save_stats["unknown_111"] = next_int_len(4);
            save_stats["unlocked_slots"] = next_int_len(1);

            int length_1 = next_int(4);
            int length_2 = next_int(4);
            Dictionary<string,dynamic> unknown_20 = new Dictionary<string,dynamic>();
            unknown_20.Add("Value", get_length_data(4, 4, length_1 * length_2));
            unknown_20["Length_1"] = length_1;
            unknown_20["Length_2"] = length_2;
            save_stats["unknown_20"] = unknown_20;

            save_stats["time_stamps_2"] = get_length_doubles(length:4);

            save_stats["trade_progress"] = next_int_len(4);

            if (save_stats["dst"] == true)
            {
                ((List<double>)save_stats["time_stamps_2"]).Add(get_double());
                save_stats["unknown_24"] = generate_empty_len(4);
            }
            else save_stats["unknown_24"] = next_int_len(4);

            save_stats["catseye_related_data"] = get_cat_upgrades();
            save_stats["unknown_22"] = get_length_data(length:11);
            save_stats["user_rank_rewards"] = get_length_data(4, 1);

            if (save_stats["dst"] == false) ((List<double>)save_stats["time_stamps_2"]).Add(get_double());

            save_stats["unlocked_forms"] = get_length_data();
            save_stats["transfer_code"] = get_utf8_string();
            save_stats["confirmation_code"] = get_utf8_string();
            save_stats["transfer_flag"] = next_int_len(1);

            List<int> lengths = new List<int>() { next_int(4), next_int(4), next_int(4) };
            length = lengths[0] * lengths[1] * lengths[2];

            var stage_data_related_1 = new Dictionary<string, dynamic>
{
    {"Value", get_length_data(4, 1, length)},
    {"Lengths", lengths}
};
            save_stats["stage_data_related_1"] = stage_data_related_1;

            save_stats["event_timed_scores"] = get_event_timed_scores();
            save_stats["inquiry_code"] = get_utf8_string();
            save_stats["play_time"] = get_play_time();

            save_stats["unknown_25"] = next_int_len(1);

            save_stats["backup_state"] = next_int_len(4);

            if (save_stats["dst"] == true) save_stats["unknown_119"] = next_int_len(1);
            else save_stats["unknown_119"] = generate_empty_len(1);

            save_stats["gv_44"] = next_int_len(4);

            save_stats["unknown_120"] = next_int_len(4);

            save_stats["itf_timed_scores"] = (List<List<int>>)chunks(get_length_data(4, 4, 51 * 3), 51);

            save_stats["unknown_27"] = next_int_len(4);
            save_stats["cat_related_data_1"] = get_length_data();
            save_stats["unknown_28"] = next_int_len(1);

            save_stats["gv_45"] = next_int_len(4);
            save_stats["gv_46"] = next_int_len(4);

            save_stats["unknown_29"] = next_int_len(4);
            save_stats["lucky_tickets_1"] = get_length_data();
            save_stats["unknown_32"] = get_length_data();

            save_stats["gv_47"] = next_int_len(4);
            save_stats["gv_48"] = next_int_len(4);

            if (save_stats["dst"] == false) save_stats["energy_notice"] = next_int_len(1);
            save_stats["account_created_time_stamp"] = get_double();

            save_stats["unknown_35"] = get_length_data();
            save_stats["unknown_36"] = next_int_len(15);

            save_stats["user_rank_popups"] = next_int_len(3);

            save_stats["unknown_37"] = next_int_len(1);

            save_stats["gv_49"] = next_int_len(4);
            save_stats["gv_50"] = next_int_len(4);
            save_stats["gv_51"] = next_int_len(4);
            save_stats["cat_guide_collected"] = get_length_data(4, 1);

            save_stats["gv_52"] = next_int_len(4);

            save_stats["time_stamps_3"] = get_length_doubles(length:5);

            save_stats["cat_fruit"] = get_length_data();
            save_stats["cat_related_data_3"] = get_length_data();
            save_stats["catseye_cat_data"] = get_length_data();
            save_stats["catseyes"] = get_length_data();
            save_stats["catamins"] = get_length_data();

            save_stats["gamatoto_time_left"] = seconds_to_time((int)(get_double()));
            save_stats["gamatoto_exclamation"] = next_int_len(1);
            save_stats["gamatoto_xp"] = next_int_len(4);
            save_stats["gamamtoto_destination"] = next_int_len(4);
            save_stats["gamatoto_recon_length"] = next_int_len(4);

            save_stats["unknown_43"] = next_int_len(4);

            save_stats["gamatoto_complete_notification"] = next_int_len(4);

            save_stats["unknown_44"] = get_length_data(4, 1);
            save_stats["unknown_45"] = get_length_data(4, 12 * 4);
            save_stats["gv_53"] = next_int_len(4);

            save_stats["helpers"] = get_length_data();

            save_stats["unknown_47"] = next_int_len(1);

            save_stats["gv_54"] = next_int_len(4);

            save_stats["purchases"] = get_purchase_receipts();
            save_stats["gv_54"] = next_int_len(4);
            save_stats["gamatoto_skin"] = next_int_len(4);
            save_stats["platinum_tickets"] = next_int_len(4);

            save_stats["login_bonuses"] = get_login_bonuses();
            save_stats["unknown_49"] = next_int_len(16);
            save_stats["announcment"] = get_length_data(length:32);

            save_stats["backup_counter"] = next_int_len(4);

            save_stats["unknown_131"] = get_length_data(length:3);
            save_stats["gv_55"] = next_int_len(4);

            save_stats["unknown_51"] = next_int_len(1);

            save_stats["unknown_113"] = get_data_before_outbreaks();

            save_stats["dojo_data"] = get_dojo_data_maybe();
            save_stats["dojo_item_lock"] = next_int_len(1);
            save_stats["dojo_locks"] = get_length_data(1, 1, 2);

            save_stats["unknown_114"] = next_int_len(4);
            save_stats["gv_58"] = next_int_len(4);  // 0x3a
            save_stats["unknown_115"] = next_int_len(8);

            save_stats["outbreaks"] = get_outbreaks();

            save_stats["unknown_52"] = get_double();
            var item_schemes = new Dictionary<string, dynamic>();
            item_schemes["to_obtain_ids"] = get_length_data();
            item_schemes["received_ids"] = get_length_data();
            save_stats["item_schemes"] = item_schemes;

            save_stats["current_outbreaks"] = get_outbreaks();

            save_stats["unknown_55"] = get_mission_data_maybe();

            save_stats["time_stamp_4"] = get_double();
            save_stats["gv_60"] = next_int_len(4);

            save_stats["unknown_117"] = get_unknown_data();

            save_stats["gv_61"] = next_int_len(4);
            var data_2 = get_unlock_popups();
            save_stats["unlock_popups"] = data_2.Item1;

            save_stats["unknown_118"] = data_2.Item2;

            save_stats["base_materials"] = get_length_data();

            save_stats["unknown_56"] = next_int_len(8);
            save_stats["unknown_57"] = next_int_len(1);
            save_stats["unknown_58"] = next_int_len(4);

            save_stats["engineers"] = next_int_len(4);
            save_stats["ototo_cannon"] = get_cat_cannon_data();

            save_stats["unknown_59"] = get_data_near_ht();

            save_stats["tower"] = get_ht_it_data();
            save_stats["missions"] = get_mission_data();
            save_stats["tower_item_obtained"] = get_tower_item_obtained();
            save_stats["unknown_61"] = get_data_after_tower();

            Dictionary<string, Dictionary<string, int>> challenege = new Dictionary<string, Dictionary<string, int>>()
{
    {"Score" , next_int_len(4)},
    {"Cleared", next_int_len(1)},
};
            save_stats["challenge"] = challenege;

            save_stats["gv_67"] = next_int_len(4);  // 0x43

            save_stats["weekly_event_missions"] = get_dict(typeof(int), typeof(bool));
            save_stats["won_dojo_reward"] = next_int_len(1);
            save_stats["event_flag_update_flag"] = next_int_len(1);

            save_stats["gv_68"] = next_int_len(4);  // 0x44

            save_stats["completed_one_level_in_chapter"] = get_dict(typeof(int), typeof(int));
            save_stats["displayed_cleared_limit_text"] = get_dict(typeof(int), typeof(bool));
            save_stats["event_start_dates"] = get_dict(typeof(int), typeof(int));
            save_stats["stages_beaten_twice"] = get_length_data();

            save_stats["unknown_102"] = get_data_after_challenge();

            var lengths_ = get_uncanny_current();
            save_stats["uncanny_current"] = lengths_;
            save_stats["uncanny"] = get_uncanny_progress(lengths_);

            var total = lengths_["total"];
            save_stats["unknown_62"] = next_int_len(4);
            save_stats["unknown_63"] = get_length_data(length: (int)total);

            save_stats["unknown_64"] = get_data_after_uncanny();

            var temp = (Dictionary<string, dynamic>) save_stats["unknown_64"];
            var temp_1 = (Dictionary<string, dynamic>) temp["progress"];
            var temp_2 = (Dictionary<string, dynamic>) temp_1["Lengths"];
            var total_ = (int)temp_2["total"];

            save_stats["unknown_65"] = next_int_len(4);
            var val_61 = save_stats["unknown_65"];

            save_stats["unknown_66"] = new List<dynamic>();
            List<dynamic> unknown_66 = new List<dynamic>();
            for (int i=0; i<total_; i++)
            {
                val_61 = next_int_len(4);
                unknown_66.Add(val_61);
            }
            save_stats["unknown_66"] = unknown_66;

            int val_54 = 0x37;
            if (val_61["Value"] < 0x38)
            {
                val_54 = val_61["Value"];
            }

            save_stats["lucky_tickets_2"] = get_length_data(length:val_54);

            save_stats["unknown_67"] = new List<dynamic>();
            if (0x37 < val_61["Value"]) save_stats["unknown_67"] = get_length_data(4, 4, val_61["Value"]);

            save_stats["unknown_68"] = next_int_len(1);

            save_stats["gv_77"] = next_int_len(4);  // 0x4d

            save_stats["gold_pass"] = get_gold_pass_data();

            save_stats["talents"] = get_talent_data();
            save_stats["np"] = next_int_len(4);

            save_stats["unknown_70"] = next_int_len(1);

            save_stats["gv_80000"] = next_int_len(4); // 80000

            save_stats["unknown_71"] = next_int_len(1);

            save_stats["leadership"] = next_int_len(2);
            save_stats["officer_pass_cat_id"] = next_int_len(2);
            save_stats["officer_pass_cat_form"] = next_int_len(2);

            save_stats["gv_80200"] = next_int_len(4);  // 80200
            save_stats["filibuster_stage_id"] = next_int_len(1);
            save_stats["filibuster_stage_enabled"] = next_int_len(1);

            save_stats["unknown_74"] = get_length_data();

            save_stats["gv_80500"] = next_int_len(4);  // 80500

            save_stats["unknown_75"] = get_length_data(2, 4);

            lengths_ = get_legend_quest_current();
            save_stats["legend_quest_current"] = lengths_;
            save_stats["legend_quest"] = get_legend_quest_progress(lengths_);

            save_stats["unknown_133"] = get_length_data(4, 1, lengths_["total"]);
            save_stats["legend_quest_ids"] = get_length_data(4, 4, lengths_["stages"]);

            save_stats["unknown_76"] = get_data_after_leadership();
            save_stats["gv_80700"] = next_int_len(4);  // 80700
            if (save_stats["dst"])
            {
                save_stats["unknown_104"] = next_int_len(1);
                save_stats["gv_100600"] = next_int_len(4);
                if (save_stats["gv_100600"]["Value"] != 100600) skip(-5);
            }
            else
            {
                save_stats["unknown_104"] = generate_empty_len(1);
                save_stats["gv_100600"] = generate_empty_len(4);
            }
            save_stats["restart_pack"] = next_int_len(1);

            save_stats["unknown_101"] = get_data_after_after_leadership(save_stats["dst"]);

            save_stats["medals"] = get_medals();

            save_stats["unknown_103"] = get_data_after_medals();

            lengths_ = get_gauntlet_current();
            save_stats["gauntlet_current"] = lengths_;
            save_stats["gauntlets"] = get_gauntlet_progress(lengths_);

            save_stats["unknown_77"] = get_length_data(4, 1, lengths_["total"]);

            save_stats["gv_90300"] = next_int_len(4);  // 90300

            lengths_ = get_gauntlet_current();
            save_stats["unknown_78"] = lengths_;
            save_stats["unknown_79"] = get_gauntlet_progress(lengths_);

            save_stats["unknown_80"] = get_length_data(4, 1, lengths_["total"]);

            save_stats["enigma_data"] = get_enigma_stages();
            var data_ = get_cleared_slots();
            save_stats["cleared_slot_data"] = data_.Item1;

            save_stats["unknown_121"] = data_.Item2;

            lengths_ = get_gauntlet_current();
            save_stats["collab_gauntlets_current"] = lengths_;
            save_stats["collab_gauntlets"] = get_gauntlet_progress(lengths_);
            save_stats["unknown_84"] = get_length_data(4, 1, lengths_["total"]);

            save_stats["unknown_85"] = data_after_after_gauntlets();

            save_stats["talent_orbs"] = get_talent_orbs(save_stats["game_version"]);

            save_stats["unknown_86"] = get_data_after_orbs();

            save_stats["cat_shrine"] = get_cat_shrine_data();

            save_stats["unknown_130"] = next_int_len(4 * 5);

            save_stats["gv_90900"] = next_int_len(4);  // 90900

            save_stats["slot_names"] = get_slot_names(save_stats);
            save_stats["gv_91000"] = next_int_len(4);
            save_stats["legend_tickets"] = next_int_len(4);

            save_stats["unknown_87"] = get_length_data(1, 5);
            save_stats["unknown_88"] = next_int_len(2);

            save_stats["token"] = get_utf8_string();

            save_stats["unknown_89"] = next_int_len(1 * 3);
            save_stats["unknown_90"] = next_int_len(8);
            save_stats["unknown_91"] = next_int_len(8);

            save_stats["gv_100000"] = next_int_len(4);  // 100000
            save_stats = check_gv(save_stats, 100100);
            if (save_stats["exit"] == true) return save_stats;

            save_stats["date_int"] = next_int_len(4);

            save_stats["gv_100100"] = next_int_len(4);  // 100100
            save_stats = check_gv(save_stats, 100300);
            if (save_stats["exit"] == true) return save_stats;

            save_stats["unknown_93"] = get_length_data(4, 19, 6);

            save_stats["gv_100300"] = next_int_len(4);  // 100300
            save_stats = check_gv(save_stats, 100700);
            if (save_stats["exit"] == true) return save_stats;

            save_stats["unknown_94"] = get_data_near_end();

            save_stats["platinum_shards"] = next_int_len(4);

            save_stats["unknown_100"] = get_data_near_end_after_shards();

            save_stats["gv_100700"] = next_int_len(4);  // 100700
            save_stats = check_gv(save_stats, 100900);
            if (save_stats["exit"] == true) return save_stats;

            save_stats["aku"] = get_aku();

            save_stats["unknown_95"] = next_int_len(1 * 2);
            save_stats["unknown_96"] = get_data_after_aku();

            save_stats["gv_100900"] = next_int_len(4);  // 100900
            save_stats = check_gv(save_stats, 101000);
            if (save_stats["exit"] == true) return save_stats;

            save_stats["unknown_97"] = next_int_len(1);

            save_stats["gv_101000"] = next_int_len(4);  // 101000
            save_stats = check_gv(save_stats, 110000);
            if (save_stats["exit"] == true) return save_stats;

            save_stats["unknown_98"] = get_data_near_end_after_aku();

            save_stats["gv_110000"] = next_int_len(4);  // 110000
            save_stats = check_gv(save_stats, 110500);
            if (save_stats["exit"] == true) return save_stats;

            data = get_gauntlet_current();
            save_stats["behemoth_culling_current"] = data;
            save_stats["behemoth_culling"] = get_gauntlet_progress(data);
            save_stats["unknown_124"] = get_length_data(4, 1, data["total"]);

            save_stats["unknown_125"] = next_int_len(1);

            save_stats["gv_110500"] = next_int_len(4);  // 110500
            save_stats = check_gv(save_stats, 110600);
            if (save_stats["exit"] == true) return save_stats;

            save_stats["unknown_126"] = next_int_len(1);

            save_stats["gv_110600"] = next_int_len(4);  // 110600
            save_stats = check_gv(save_stats, 110700);
            if (save_stats["exit"] == true) return save_stats;

            save_stats["unknown_127"] = get_110700_data();

            if (save_stats["dst"] == true) save_stats["unknown_128"] = next_int_len(1);
            else save_stats["unknown_128"] = generate_empty_len(1);

            save_stats["gv_110700"] = next_int_len(4);  // 110700
            save_stats = check_gv(save_stats, 110800);
            if (save_stats["exit"] == true) return save_stats;

            save_stats["shrine_dialogs"] = next_int_len(4);

            save_stats["unknown_129"] = get_110800_data();

            save_stats["dojo_3x_speed"] = next_int_len(1);

            save_stats["unknown_132"] = get_110800_data_2();

            save_stats["gv_110800"] = next_int_len(4);  // 110800
            save_stats = check_gv(save_stats, 110900);
            if (save_stats["exit"] == true) return save_stats;

            save_stats["unknown_135"] = get_110900_data();
            save_stats["gv_110900"] = next_int_len(4);  // 110900
            save_stats = check_gv(save_stats, 120000);
            if (save_stats["exit"] == true) return save_stats;

            save_stats["zero_legends"] = get_zero_legends();
            save_stats["unknown_136"] = next_int_len(1);
            save_stats["gv_120000"] = next_int_len(4);  // 120000
            save_stats = check_gv(save_stats, 120100);
            if (save_stats["exit"] == true) return save_stats;

            save_stats["unknown_137"] = get_120100_data();
            save_stats["gv_120100"] = next_int_len(4);  // 120100
            save_stats = check_gv(save_stats, 120200);
            if (save_stats["exit"] == true) return save_stats;

            save_stats["unknown_138"] = get_120200_data();
            save_stats["gv_120200"] = next_int_len(4);  // 120200
            save_stats = check_gv(save_stats, 120200);
            if (save_stats["exit"] == true) return save_stats;

            length = save_data.Length - address - 32;
            save_stats["extra_data"] = next_int_len(length);

            save_stats = exit_parser(save_stats);
            
            return save_stats;
        }

    }
}