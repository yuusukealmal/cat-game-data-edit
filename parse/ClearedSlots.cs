﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCSFE.parse
{
    internal class ClearedSlots
    {
        public class Slot
        {
            public class Cat
            {
                public int cat_id;
                public int cat_form;

                public Cat(int cat_id, int cat_form)
                {
                    this.cat_id = cat_id;
                    this.cat_form = cat_form;
                }

                public Dictionary<string, dynamic> to_dict()
                {
                    return new Dictionary<string, dynamic>()
                {
                    {"cat_id", cat_id},
                    {"cat_form", cat_form}
                };
                }

                public static Cat from_dict(Dictionary<string, dynamic> data)
                {
                    return new Cat((int)data["cat_id"], (int)data["cat_form"]);
                }
            }

            public List<Cat> cats;
            public int slot_index;
            public int separator;

            public Slot(List<Cat> cats, int slot_index, int separator)
            {
                this.cats = cats;
                this.slot_index = slot_index;
                this.separator = separator;
            }

            public Dictionary<string, dynamic> to_dict()
            {
                List<dynamic> cat_list = new List<dynamic>();
                foreach (Cat cat in cats)
                {
                    cat_list.Add(cat.to_dict());
                }

                return new Dictionary<string, dynamic>()
            {
                {"cats", cat_list},
                {"slot_index", slot_index},
                {"separator", separator}
            };
            }

            public static Slot from_dict(Dictionary<string, dynamic> data)
            {
                List<Cat> cat_list = new List<Cat>();
                foreach (Dictionary<string, dynamic> cat_dict in (List<dynamic>)data["cats"])
                {
                    cat_list.Add(Cat.from_dict(cat_dict));
                }

                return new Slot(cat_list, (int)data["slot_index"], (int)data["separator"]);
            }
        }
        public class StageSlot
        {
            public class Stage
            {
                public int stage_id;
                public Stage(int stage_id)
                {
                    this.stage_id = stage_id;
                }

                public Dictionary<string, dynamic> to_dict()
                {
                    return new Dictionary<string, dynamic>
                {
                    { "stage_id", stage_id }
                };
                }

                public static Stage form_dict(Dictionary<string, dynamic> data)
                {
                    return new Stage((int)data["stage_id"]);
                }
            }

            public int slot_index;
            public List<Stage> stages;

            public StageSlot(int slot_index, List<Stage> stages)
            {
                this.slot_index = slot_index;
                this.stages = stages;
            }

            public Dictionary<string, dynamic> to_dict()
            {
                return new Dictionary<string, dynamic>
            {
                { "slot_index", slot_index },
                { "stages", stages.ConvertAll(stage => stage.to_dict()) }
            };
            }

            public static StageSlot from_dict(Dictionary<string, dynamic> data)
            {
                return new StageSlot
                (
                    (int)data["slot_index"],
                    ((List<dynamic>)data["stages"]).ConvertAll(stage => Stage.form_dict((Dictionary<string, dynamic>)stage))
                );
            }
        }

        public List<Slot> slots;
        public List<StageSlot> slot_stages;
        public int end_index;

        public ClearedSlots(List<Slot> slots, List<StageSlot> slot_stages, int end_index)
        {
            this.slots = slots;
            this.slot_stages = slot_stages;
            this.end_index = end_index;
        }
        public Dictionary<string, dynamic> to_dict()
        {
            return new Dictionary<string, dynamic>
        {
            { "slots", slots.ConvertAll(slot => slot.to_dict()) },
            { "slot_stages", slot_stages.ConvertAll(stage => stage.to_dict()) },
            { "end_index", end_index }
        };
        }

        public static ClearedSlots FromDict(Dictionary<string, dynamic> data)
        {
            return new ClearedSlots
            (
                ((List<dynamic>)data["slots"]).ConvertAll(slot => Slot.from_dict((Dictionary<string, dynamic>)slot)),
                ((List<dynamic>)data["slot_stages"]).ConvertAll(stage => StageSlot.from_dict((Dictionary<string, dynamic>)stage)),
                (int)data["end_index"]
            );
        }
    }
}
