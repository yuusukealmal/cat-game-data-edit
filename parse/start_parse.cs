﻿using BCSFE.parse;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BCSFE.data_download
{
    internal class start_parse
    {
        public static Dictionary<string,object> Start_parse(byte[] save_data, string cc)
        {
            Dictionary<string, dynamic> save_stats = new Dictionary<string,dynamic>();
            save_stats = Parse_save.parse_save(save_data, cc);
            //try
            //{
            //    save_stats = Parse_save.parse_save(save_data, cc);
            //    int i = 1;
            //}
            //catch (Exception e)
            //{
            //    File.WriteAllText("error.txt", e.ToString());
            //    var game_version = Parse_save.get_game_version(save_data);
            //    if (game_version < 110000)
            //    {
            //        MessageBox.Show("This save is from before 11.0.0\nThis might be the problem");
            //    }
            //    MessageBox.Show("Error happends, write log to error.txt");
            //}
            return save_stats;
        }
    }
}
